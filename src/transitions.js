import Vue from 'vue'

export default {
  init () {
    Vue.transition('fade', {
      enterClass: 'fadeInRight',
      leaveClass: 'fadeOutRight'
    })

    Vue.transition('panel-slide', {
      enterClass: 'slideInDown',
      leaveClass: 'slideOutUp'
    })
  }
}
