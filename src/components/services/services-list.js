export default [
  {
    title: '_.home.services.internationalTitle',
    body: '_.home.services.internationalTxt',
    icon: 'fa-globe',
    link: 'international'
  },
  {
    title: '_.home.services.nationalTitle',
    body: '_.home.services.nationalTxt',
    icon: 'fa-truck',
    link: 'national'
  },
  {
    title: '_.home.services.transpTitle',
    body: '_.home.services.transpTxt',
    icon: 'fa-plane',
    link: 'transportation'
  },
  {
    title: '_.home.services.storageTitle',
    body: '_.home.services.storageTxt',
    icon: 'fa-cube',
    link: 'storage'
  },
  {
    title: '_.home.services.artTitle',
    body: '_.home.services.artTxt',
    icon: 'fa-paint-brush',
    link: 'artworks'
  },
  {
    title: '_.home.services.extraTitle',
    body: '_.home.services.extraTxt',
    icon: 'fa-map-signs',
    link: 'extras'
  },
  {
    title: '_.home.services.petsTitle',
    body: '_.home.services.petsTxt',
    icon: 'fa-paw',
    link: 'pets'
  },
  {
    title: '_.home.services.relocationTitle',
    body: '_.home.services.relocationTxt',
    icon: 'fa-home',
    link: 'relocation'
  }
]
