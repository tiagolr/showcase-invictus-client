/*eslint-disable quotes*/
/*eslint-disable indent*/
/*eslint-disable key-spacing*/
export default {
	"locales": {
		"current":{"en":"true", "lang":"English"}
	},
	navbar: {
		"home":"Home",
		"about":"About Us",
		"services":"Services",
		"facilities":"Facilities",
		"materials":"Materiais",
		"contacts":"Contacts",
		"budget":"Budget",
		"social":"Social Networks",
		"tracking":"Tracking",
		"gallery":"Gallery"
	},
	misc: {
		"close": "Close",
		"cancel":"Cancel",
		"seeAlso": "Also see"
	},
	'welcomeTxt': 'Welcome to',
	"ABOUT_US":"Invictus Relocation is the new choice of relocation services, we provide services with highest quality in the field.",
	home: {
		services: {
			"title":"Our Services",
			"internationalTitle":"International Moves",
			"internationalTxt":"We make the international moving process a simple and convenient task.",
			"nationalTitle":"National Moves",
			"nationalTxt":"Invictus offers several solutions for change within the country.",
			"logistics":"Logistics",
			"logisticsTitle":"Logistics",
			"logisticsTxt":"In Invictus we put at their disposal all the logistics linked to the transport of all your goods.",
			"transpTitle":"Air Sea and Road Transportation",
			"transpTxt":"Different means of transport are available.",
			"storageTitle":"Storage",
			"storageTxt":"Safety and quality are things we look for in storage equipment.",
			"officeTitle":"Office moves",
			"officeTxt":"Transfer your office to another location quickly and smoothly.",
			"artTitle":"Art Transportation",
			"artTxt":"Transport of works of art and other fragile goods is made with care.",
			"extraTitle":"Extra Services",
			"extraTxt":"A wide range of supplementary services is also available.",
			"petsTitle":"Pet Moving",
			"petsTxt":"We handle the transport of your pets with the utmost care.",
			"relocationTitle":"Relocation",
			"relocationTxt":"Invicuts offers a complete relocation service."
		}
	},
	"INFO_KNOW_MORE":"Know More",
	"FACILITIES_TITLE":"Our Facilities",
	"FACILITIES_TXT_SM":"Our facilities are well located and have the equipment and facilities necessary to meet diverse logistics operations.",
	"MATERIALS_TITLE":"Our Materials",
	"MATERIALS_TXT_SM":"The materials used in the transport and storage of orders must be appropriate, for this we have a wide range of solutions.",
	"CONTACTS_TITLE":"Contacts",
	inquiry: {
		"budget": "Fill our form and receive a custom budget for free.",
		"budgetBtn": "Request Budget",
		form: {
			"title":"Online Quote",
			"name":"Name*",
			"email":"Email*",
			"phone":"Phone*",
			"origin":"Origin*",
			"destination":"Destiny*",
			"date":"Move Date*",
			"comments":"Observations",
			"antispam":" ",
			"antispamTxt":"I confirm that the data entered is correct.",
			"send":"Send",
			placeholders: {
				"origin":"Lisbon, Portugal",
				"name":"Joe Steven",
				"email":"example@email.com",
				"destination":"Paris, France",
				"phone":"+351 808 910 100",
				"comments":"Put here comments or additional information in relation to your request."
			}
		}
	},
	tracking: {
		title: 'Relocation Tracking'
	},
	"SOCIAL_TITLE": "Find us also on social-networks.",
	"CERTIFIED_BY": "Certified by:",
	"TRACKING_TITLE": "Move Tracking",
	"TRACKING_TXT": "If you already have an order, enter your <strong>tracking code</strong> here to check its status and location.",
	"TRACKING_FIND": "Find",
	"TRACKING_STATE": "State",
	"TRACKING_LOCAL": "Local",
	"TRACKING_PLACEHOLDER":"Tracking Code",
	"GALLERY_TITLE":"Gallery",
	"INFO_NATIONAL_TITLE":"National Moves",
	"INFO_INTERNATIONAL_TITLE":"International Moves",
	"INFO_EXTRAS_TITLE":"Extra Services",
	"INFO_LOGISTICS_TITLE":"Logistics",
	"INFO_TRANSPORTATION_TITLE":"Transportation",
	"INFO_STORAGE_TITLE":"Storage",
	"INFO_OFFICE_TITLE":"Office Moves",
	"INFO_ART_TITLE":"Artwork transportation",
	"INFO_PETS_TITLE":"Pet Moving",
	"INFO_RELOCATION_TITLE":"Relocation",
	"INFO_TXT_MATERIALS":`
<li>Boxes with hangers for clothes</li>
<li>Filling Foam</li>
<li>Paper cover</li>
<li>Wooden crates</li>
<li>Frame boxes</li>
<li>Boxes with dividers for glasses</li>
<li>Wine boxes</li>
<li>Bubble Wrap</li>
<li>Wooden boxes for valuable crockery</li>
<li>Boxes for seats</li>
<li>Boxes for books</li>
<li>High boxes appropriated for lamps</li>
<li>Steel Strap</li>
<li>“Magic Box”</li>
<li>Boxes for lampshades</li>
<li>Display tubes</li>
<li>Wrapping paper for crockery and decoration items</li>
<li>Blankets</li>
`,
	"INFO_TXT_FACILITIES":`
<p> Our facilities are located in a private condominium with video surveillance system and image recording. </p>
<p> The whole area is covered by fire, smoke and CO2 detectors connected to a central alarm station, police and fire departments within walking distance and fire-fighting systems. </p>
<p> Large area for loading and unloading cargo as well as parking slots for our customers. </p>
<p> Close to the main access roads. ( A5 / IC19 / A16 ) </p>
`,
	"INFO_TXT_INTERNATIONAL":`
<p> Choose a place to go, Invictus Relocation takes care of the rest. </p>
<p> Choosing the right method of transport is a vital part of the international change process. As such, Invictus will elect the more appropriate means of transport according to each customer's needs. By air, sea or land your cargo is safe. </p>
<p> <strong> We plan your move: </strong> </p>
<p> Invictus will appoint a marketing executive to make a visit to the customer's home, to make a survey of customer needs, check the volume to carry and determine the type of packaging materials to be used to ensure a quality service. </p>
<p> <strong> Our services include: </strong> </p>
<ul>
<li> Survey with the client</li>
<li> Volume Verification </li>
<li> Access Verification / conditions for loading / unloading </li>
<li> Protection of the house floor and interior</li>
<li> Export Packing </li>
<li> Load Inventory Carrying </li>
<li> Labeling each volume </li>
</ul>
<p> <strong> Delivery Destination: </strong> </p>
<p>
Invictus will inform you about all the documentation required for customs in the destination country. Through our worldwide network of agents, we are able to deliver your belongings in any country in the world.
</p>
`,
	"INFO_TXT_NATIONAL":`
<p> Choose a place to go, the Invictus Relocation takes care of the rest. </p>
<p> <strong> We plan your move: </strong> </p>
<p> Invictus will appoint a marketing executive to make a visit to the customer's home, to make a survey of customer needs, check the volume to carry and determine the type of packaging materials to be used to ensure a quality service. </p>
<p> <strong> Our services include: </strong> </p>
<ul>
<li> Survey with the client </li>
<li> Volume Verification </li>
<li> Access Verification / conditions for loading / unloading </li>
<li> Protection of the house floor and interior </li>
<li> Packing </li>
<li> Load Inventory Carrying </li>
<li> Labeling each volume </li>
</ul>

<p> <strong> Delivery Destination: </strong> </p>
<p> Delivery will be effected, unpacked, and furniture will be installed if requested by the customer. </p>
`,
	"INFO_TXT_LOGISTICS":`
<p>The Invictus offers you several options of logistics: </p>

<ul>
<li> By Road: For intra European services</li>
<li>By Air:. For faster delivery </li>
<li>By Sea: For International Services </li>
<li>LCL (Groupage Services):. Use Partial containers for smaller expeditions</li>
<li>FCL (Full Container Load):. Only use containers</li>
</ul>
`,
	"INFO_TXT_TRANSPORTATION":`
<p>
The method of transport is a vital part of the change process. Invictus deals with the client what is the most appropriate shipping method.
By air, sea or land their cargo is safe.
</p>
`,
	"INFO_TXT_STORAGE":`
<p> Invictus has at its disposal appropriate storage infrastructures and so we can store your goods in a safe and careful manner.
</p>
<p> <strong> The storage solutions are: </strong> </p>
<ul>
<li> Short term storage </li>
<li> Long-term storage </li>
<li> Car Storage </li>
<li> Transit cargo storage </li>
</ul>
`,
	"INFO_TXT_OFFICE":`
<p> Your company is thinking about changing office? </p>
<p> In Invictus we offer solutions for quick and thoughtful change, so that your work does not stop. </p>
`,
	"INFO_TXT_ARTWORKS":`
<p>
Your own a work of art whose value is priceless? Consider changing is worrisome? In Invictus you will find specialized personnel for the transport of all types
of art works, ensuring that the process that seemed complicated is very straightforward.
</p>
`,
	"INFO_TXT_EXTRAS":`
<p> As each change is unique, in Invictus we offer a wide range of extra services to meet all your personal needs: </p>
<ul>
<li> Transport pianos / tuning </li>
<li> Unwanted goods Removal </li>
<li> Donations to charitable organizations </li>
<li> Transport vehicles </li>
<li> Cleaning Services </li>
<li> Carpenter service, plumber, electrician </li>
<li> Furniture Restoration </li>
</ul>
`,
	"INFO_TXT_PETS":`
<p> We know that pets are like family members, so their transport can
worry owners. However, with our service
of animals transport your
companion will be transported safely and will
come with optimum health. </p>
`,
	"INFO_TXT_RELOCATION":`
<p>
We know that moving to a new country, new culture, new language, is a task that can
look complicated and still brings concerns like how to find the ideal home,
transportation, school for the children, etc. </p>

<p> Invictus Relocation offers guests a full-service Relocation in which they are
include the following services: </p>
<ul>
<li> Treatment Visa / Immigration </li>
<li> Local Guidance </li>
<li> Temporary Accommodation </li>
<li> School Search </li>
<li> Residence Search </li>
<li> Babysitting </li>
<li> Furniture Rental </li>
<li> Healthcare </li>
<li> Car Register </li>
<li> Taxi Service </li>
</ul>
`
}
