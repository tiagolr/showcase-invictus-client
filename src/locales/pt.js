/*eslint-disable quotes*/
/*eslint-disable indent*/
/*eslint-disable key-spacing*/
export default {
	'locales': {
		'current':{'pt':'true', 'lang':'Português'}
	},
	navbar: {
		"home":"Home",
		'about':'Sobre Nós',
		'services':'Serviços',
		'facilities':'Instalações',
		'materials':'Materiais',
		'contacts':'Contactos',
		'budget':'Orçamento',
		'social':'Redes Sociais',
		'tracking':'Tracking',
		'gallery':'Galeria'
	},
	misc: {
		'close': 'Fechar',
		'cancel':'Cancelar',
		"seeAlso": "Veja também"
	},
	'welcomeTxt': 'Bem Vindo à',
	'ABOUT_US':'A Invictus Relocation é a nova escolha nos serviços de mudança, fornecemos serviços com qualidade e ao mais alto nível do setor.',
	home: {
		services: {
			'title':'Os Nossos Serviços',
			'internationalTitle':'Mudanças Internacionas',
			'internationalTxt':'Tornamos o processo de mudança internacional numa tarefa simples e cómoda.',
			'nationalTitle':'Mudanças Nacionais',
			'nationalTxt':'Oferecemos várias soluções para mudanças dentro de território nacional.',
			'logisticsTitle':'Soluções de Logística',
			'logisticsTxt':'Na Invictus pomos à sua disposição toda a logística ligada ao transporte de todos os seus bens.',
			'transpTitle':'Transportes Aéreos Marítimos e Terrestres',
			'transpTxt':'Colocamos à sua escolha diferentes meios de transporte para a sua encomenda.',
			'storageTitle':'Armazenagem',
			'storageTxt':'Fornecemos soluções de armazenagem com qualidade e segurança ao mais alto nível.',
			'officeTitle':'Mudanças de Escritório',
			'officeTxt':'Transferimos o seu escritório para outro local de forma rápida e cuidadosa.',
			'artTitle':'Transporte de Obras de Arte',
			'artTxt':'Asseguramos o cuidado no transporte de obras de arte e de outros bens frágeis.',
			'extraTitle':'Serviços Extra',
			'extraTxt':'Dispomos ainda uma vasta gama de serviços complementares.',
			'petsTitle':'Transporte de Animais de Estimação',
			'petsTxt':'Tratamos do transporte dos seus animais de estimação com o maior cuidado.',
			'relocationTitle':'Relocation',
			'relocationTxt':'Colocamos ao seu dispor um serviço completo de Relocation.'
		}
	},
	'INFO_KNOW_MORE':'Saber Mais',
	'FACILITIES_TITLE':'Instalações',
	'FACILITIES_TXT_SM':'As nossas instalações encontram-se bem situadas e possuem o equipamento e as condições necessárias para atender as diversas operações de logística.',
	'MATERIALS_TITLE':'Equipamento',
	'MATERIALS_TXT_SM':'Os materiais usados no transporte e armazenamento das encomendas devem ser apropriados, para isso Dispomos de a uma grande variedade de soluções.',
	'CONTACTS_TITLE':'Contactos',
	inquiry: {
		'budget': 'Preencha o nosso formulário e receba gratuitamente um orçamento personalizado.',
		'budgetBtn': 'Pedir Orçamento',
		form: {
			'title':'Orçamento Online',
			'name':'Nome',
			'email':'Email',
			'phone':'Telefone',
			'origin':'Origem',
			'destination':'Destino',
			'date':'Data da Mudança',
			'comments':'Observações',
			'antispam':' ',
			'antispamTxt':'Confirmo que os dados inseridos estão corretos.',
			'send':'Enviar',
			placeholders: {
				'name':'António José',
				'email':'exemplo@email.com',
				'phone':'+351 808 910 100',
				'origin':'Lisboa, Portugal',
				'destination':'Paris, França',
				'comments':'Coloque aqui informações adicionais que considere uteis para que possamos atender o seu pedido.'
			}
		}
	},
	tracking: {
		title: 'Localizar Mudança'
	},
	'SOCIAL_TITLE': 'Encontre-nos também nas redes-sociais',
	'CERTIFIED_BY': 'Certificados por:',
	'TRACKING_TITLE': 'Localize a sua mudança',
	'TRACKING_TXT': 'Insira aqui o seu <strong>código de localização</strong> para consultar o estado e localização da sua mudança.',
	'TRACKING_FIND': 'Procurar',
	'TRACKING_STATE': 'Estado',
	'TRACKING_LOCAL': 'Local',
	'TRACKING_PLACEHOLDER':'Código de Tracking',
	'GALLERY_TITLE':'Galeria',
	services: {
		'INFO_NATIONAL_TITLE':'Mudanças Nacionais',
		'INFO_INTERNATIONAL_TITLE':'Mudanças Internacionais',
		'INFO_EXTRAS_TITLE':'Serviços Extras',
		'INFO_LOGISTICS_TITLE':'Soluções de logística',
		'INFO_TRANSPORTATION_TITLE':'Transportes',
		'INFO_STORAGE_TITLE':'Armazenagem',
		'INFO_OFFICE_TITLE':'Mudanças de Escritório',
		'INFO_ART_TITLE':'Transporte de Obras de Arte',
		'INFO_PETS_TITLE':'Transporte de animais de Estimação',
		'INFO_RELOCATION_TITLE':'Relocation',
		'INFO_TXT_MATERIALS': `TODO!!!`
	},
	'INFO_NATIONAL_TITLE':'Mudanças Nacionais',
	'INFO_INTERNATIONAL_TITLE':'Mudanças Internacionais',
	'INFO_EXTRAS_TITLE':'Serviços Extras',
	'INFO_LOGISTICS_TITLE':'Soluções de logística',
	'INFO_TRANSPORTATION_TITLE':'Transportes',
	'INFO_STORAGE_TITLE':'Armazenagem',
	'INFO_OFFICE_TITLE':'Mudanças de Escritório',
	'INFO_ART_TITLE':'Transporte de Obras de Arte',
	'INFO_PETS_TITLE':'Transporte de animais de Estimação',
	'INFO_RELOCATION_TITLE':'Relocation',
	'INFO_TXT_MATERIALS':`
<li>Caixas de pendurados para colocação de roupa</li>
<li>Esferovite de enchimento</li>
<li>Manta de papel</li>
<li>Caixa de madeir</li>
<li>Caixas de quadros</li>
<li>Caixas com divisórias para copos</li>
<li>Caixas de vinho</li>
<li>Papel bolha</li>
<li>Caixa de madeira para loiças valiosas</li>
<li>Caixa para cadeiras</li>
<li>Caixa para livros</li>
<li>Caixa alta apropriada para candeeiros</li>
<li>Cinta de aço</li>
<li>“Magic Box”</li>
<li>Caixa para abajur</li>
<li>Tubos para tela</li>
<li>Papel para loiça</li>
<li>Mantas</li>
`,
	'INFO_TXT_FACILITIES':`
<p>As nossas instalações localizam-se num condomínio fechado, com sistema de vídeo vigilância,
com gravação de imagens.</p>

<p>Toda a área encontra-se coberta por detetores de incêndio, fumo e CO2, com comunicação
direta à Central de alarmes, GNR e Bombeiros, bem como meios próprios de extinção de
incêndio.</p>

<p>Amplo logradouro para cargas e descargas, bem como parque de estacionamento para os nossos clientes.</p>

<p>Próximo às principais vias de acesso – A5 / IC19 / A16.</p>
`,
	'INFO_TXT_INTERNATIONAL':`
<p>Escolha o sítio para onde vai morar, a Invictus Relocation trata do resto.</p>
<p>A escolha do método correto de transporte é uma parte vital do processo da mudança internacional. Como tal, a Invictus Relocation irá eleger (de acordo com as necessidades de cada cliente) o meio de transporte mais adequado. Pelo ar, terra ou mar a sua carga está segura. </p>
<p><strong>Planeamos a sua mudança: </strong></p>
<p>A Invictus designará um responsável comercial para fazer uma visita ao domicílio do cliente, para efetuar o levantamento das necessidades do cliente, verificar o volume a transportar e determinar o tipo de materiais de embalagem a utilizar para assegurar um serviço de qualidade. </p>
<p><strong>Os nossos serviços incluem:</strong></p>
<ul>
<li>Inquérito junto do cliente</li>
<li>Verificação do volume</li>
<li>Verificação do acesso / condição para carga/descarga</li>
<li>Proteção do chão e espaço da casa </li>
<li>Embalagem de Exportação</li>
<li>Realização de Inventário da Carga</li>
<li>Etiquetagem de cada volume</li>
</ul>
<p><strong>Entrega no Destino:</strong></p>
<p>
A Invictus irá informá-lo sobre toda a documentação necessária para desalfandegar os seus bens no país de destino. Através de nossa rede mundial de agentes, estamos capacitados em entregar sua mudança em qualquer país do mundo.
</p>
`,
	'INFO_TXT_NATIONAL':`
<p>Escolha o sítio para onde vai morar, a Invictus Relocation trata do resto.</p>
<p><strong>Planeamos a sua mudança: </strong></p>
<p>A Invictus designará um responsável comercial para fazer uma visita ao domicílio do cliente, para efetuar o levantamento das necessidades do cliente, verificar o volume a transportar e determinar o tipo de materiais de embalagem a utilizar para assegurar um serviço de qualidade. </p>
<p><strong>Os nossos serviços incluem:</strong></p>
<ul>
<li>Inquérito junto do cliente</li>
<li>Verificação do volume</li>
<li>Verificação do acesso / condição para carga/descarga</li>
<li>Proteção do chão e espaço da casa </li>
<li>Embalagem </li>
<li>Realização de Inventário da Carga</li>
<li>Etiquetagem de cada volume</li>
</ul>

<p><strong>Entrega no Destino:</strong></p>
<p>Será efetuada a entrega, desembalagem, e montagem dos móveis (caso seja solicitado pelo cliente).</p>
`,
	'INFO_TXT_LOGISTICS':`
<p>A Invictus oferece-lhe diversas opções de logistica:</p>

<ul>
<li>Via Rodoviária: Para Serviços intra europeus.</li>
<li>Via Aérea: Para uma entrega mais rápida.</li>
<li>Via Marítima: Para Serviços Internacionais </li>
<li>LCL (Groupage Services) : Uso Parcial de Contentores para expedições menores.</li>
<li>FCL (Full Container Load): Uso Exclusivo de Contentores.</li>
</ul>
`,
	'INFO_TXT_TRANSPORTATION':`
<p>
O método de transporte é uma parte vital do processo de mudança. A Invictus tratará com o cliente qual o método de transporte adequado.
Pelo ar, terra ou mar a sua carga está segura.
</p>
`,
	'INFO_TXT_STORAGE':`
<p>A Invictus tem ao seu dispor infraestruturas de armazenagem adequadas e
	próprias para que possa guardar os seus bens de forma segura e cuidada.
</p>
<p><strong>As soluções de armazenagem são:</strong></p>
<ul>
<li>Armazenagem a curto prazo</li>
<li>Armazenagem a longo prazo</li>
<li>Armazenagem de viaturas</li>
<li>Armazenagem de cargas em trânsito</li>
</ul>
`,
	'INFO_TXT_OFFICE':`
<p>A sua empresa está a pensar em mudar de escritório?</p>
<p>Na Invictus oferecemos soluções para uma mudança rápida e segura, de forma a que o seu trabalho não pare.</p>
`,
	'INFO_TXT_ARTWORKS':`
<p>
Tem uma obra de arte cujo valor é inestimável? Pensar em mudar é preocupante? Na Invictus encontrará pessoal especializado no transporte de todo o tipo
de obras de arte, assegurando que o processo que parecia complicado se torna bastante simples.
</p>
`,
	'INFO_TXT_EXTRAS':`
<p>Como cada mudança é única, na Invictus oferecemos uma vasta gama de serviços extras de forma a satisfazer todas as suas necessidades pessoais: </p>

<ul>
<li>Transporte de pianos / afinação</li>
<li>Remoção de bens indesejados </li>
<li>Doações para organizações de caridade</li>
<li>Transporte de veiculos</li>
<li>Serviços de limpeza</li>
<li>Serviço de carpinteiro, canalizador, eletricista</li>
<li>Restauração de mobiliario</li>
</ul>
`,
	'INFO_TXT_PETS':`
<p> Sabemos que os animais de estimação são como membros da família, pelo que o seu transporte pode preocupar os donos.
Contudo, com os nossos serviços de transporte de animais tem a garantia de que o seu companheiro será transportado em segurança e de que
chegará com óptima saúde.</p>
`,
	'INFO_TXT_RELOCATION':`
<p>
Sabemos que a mudança para um novo país, nova cultura, nova língua, é uma tarefa que pode
parecer complicada e acarreta ainda outras preocupações como encontrar a residência ideal,
transporte, escola para os filhos, etc.</p>

<p>A Invictus Relocation coloca ao seu dispor um serviço completo de Relocation no qual estão
incluídos, por exemplo, os seguintes serviços:</p>

<ul>
<li>Tratamento de Vistos / Imigração</li>
<li>Orientação Local</li>
<li>Alojamento Temporário</li>
<li>Procura de Escola</li>
<li>Procura de Residência</li>
<li>Serviço de Babysitter</li>
<li>Aluguer de Mobiliário</li>
<li>Cuidados Médicos</li>
<li>Registo de Viaturas</li>
<li>Serviço de Táxi</li>
</ul>
`
}
