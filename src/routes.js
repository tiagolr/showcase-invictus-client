import Home from './components/home/Home'
import About from './components/about/About'
import Gallery from './components/about/Gallery.vue'
import FAQ from './components/about/FAQ.vue'
import Company from './components/about/Company.vue'
import Inquiry from './components/inquiry/Inquiry'
import Tracking from './components/tracking/Tracking'

// services
import Services from './components/services/Services'
import EmptyComponent from './components/EmptyComponent.vue'
import Artworks from './components/services/Artworks'
import Extras from './components/services/Extras'
import International from './components/services/International'
import Logistics from './components/services/Logistics'
import National from './components/services/National'
import Office from './components/services/Office'
import Pets from './components/services/Pets'
import Relocation from './components/services/Relocation'
import Storage from './components/services/Storage'
import Transportation from './components/services/Transportation'

export default {
  '/': { name: 'home', component: Home
  },
  '/about': { name: 'about', component: About, subRoutes: {
    '/': {component: EmptyComponent},
    '/faqs': { name: 'faqs', component: FAQ },
    '/company': { name: 'company', component: Company },
    '/gallery': { name: 'gallery', component: Gallery }}
  },
  '/services': { name: 'services', component: Services, subRoutes: {
    '/': {component: EmptyComponent},
    '/artworks': { name: 'artworks', component: Artworks, title: '_.INFO_ART_TITLE' },
    '/extras': { name: 'extras', component: Extras, title: '_.INFO_EXTRAS_TITLE' },
    '/international': { name: 'international', component: International, title: '_.INFO_INTERNATIONAL_TITLE' },
    '/logistics': { name: 'logistics', component: Logistics, title: '_.INFO_LOGISTICS_TITLE' },
    '/national': { name: 'national', component: National, title: '_.INFO_NATIONAL_TITLE' },
    '/office': { name: 'office', component: Office, title: '_.INFO_OFFICE_TITLE' },
    '/pets': { name: 'pets', component: Pets, title: '_.INFO_PETS_TITLE' },
    '/relocation': { name: 'relocation', component: Relocation, title: '_.INFO_RELOCATION_TITLE' },
    '/storage': { name: 'storage', component: Storage, title: '_.INFO_STORAGE_TITLE' },
    '/transportation': { name: 'transportation', component: Transportation, title: '_.INFO_TRANSPORTATION_TITLE' }}
  },
  '/quote': {name: 'quote', component: Inquiry},
  '/tracking': {name: 'tracking', component: Tracking}
}
