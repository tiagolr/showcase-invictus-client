/* eslint-disable no-new */
import Vue from 'vue'
import App from './App'
import VueI18n from 'vue-i18n'
import LocalesPt from 'src/locales/pt.js'
import LocalesEn from 'src/locales/en.js'
import Router from 'vue-router'
import routes from './routes'
import transitions from './transitions.js'

new WOW().init()  // eslint-disable-line

// Locales setup
let locales = {
  pt: {
    _: LocalesPt
  },
  en: {
    _: LocalesEn
  }
}
Vue.use(VueI18n)
Vue.config.lang = 'pt'
Object.keys(locales).forEach(function (lang) {
  Vue.locale(lang, locales[lang])
})
//

// Transitions setup
transitions.init()

// Router setup
Vue.use(Router)
let router = new Router({
  history: true,
  saveScrollPosition: true
})
router.map(routes)
router.beforeEach(function (transition) {
  if (transition.from.name !== transition.to.name) {
    window.scrollTo(0, 0)
  }
  // set language from query
  let newLang = transition.to.query && transition.to.query.lang
  if (Vue.config.lang !== newLang && newLang) {
    Vue.config.lang = newLang
  }
  transition.next()
})
router.start(App, '#root')
//

// Vue.config.debug = true
Vue.nextTick(() => {
  $.scrollUp({scrollImg: true})
})
