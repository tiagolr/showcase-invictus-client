(function (console) { "use strict";
var $estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Client = function() { };
Client.__name__ = true;
Client.main = function() {
	js.JQuery("document").ready(function(_) {
		Client.init();
	});
};
Client.init = function() {
	js.JQuery(".carousel").carousel({ interval : 5000});
	Client.documentLanguage = js.JQuery("html").attr("lang").toLowerCase();
	Client.initBudgetForm();
	Client.initTrackingForm();
};
Client.initBudgetForm = function() {
	var beforeBudgetSubmit = function() {
		js.JQuery("#btnBFSubmit").attr("disabled",true).html("A Enviar...");
	};
	var budgetFormOptions = { resetForm : true, beforeSubmit : beforeBudgetSubmit, success : Client.onBudgetSubmited, error : Client.onBudgetError, timeout : 3000};
	js.JQuery("#budgetForm").ajaxForm(budgetFormOptions);
};
Client.onBudgetSubmited = function(data,status) {
	console.log(data);
	js.JQuery("#btnBFSubmit").attr("disabled",false).html("Enviar Pedido");
	js.JQuery("#budgetDialog").modal("hide");
	var errorMsg = "";
	var btnRequestBudget = js.JQuery("#btnRequestBudget");
	btnRequestBudget.removeClass("btn-primary");
	btnRequestBudget.removeClass("btn-danger");
	btnRequestBudget.addClass("btn-success");
	if(Client.documentLanguage == "en") btnRequestBudget.html("Request Sent"); else btnRequestBudget.html("Pedido Enviado");
	if(Client.documentLanguage == "en") errorMsg = "Thanks, your request has been sent and will be answered soon."; else errorMsg = "Obrigado, o seu pedido foi enviado e será respondido em breve.";
	utils_Utils.flashMessage(errorMsg,"success");
};
Client.onBudgetError = function(data) {
	console.log(data);
	js.JQuery("#btnBFSubmit").attr("disabled",false).html("Enviar Pedido");
	js.JQuery("#budgetDialog").modal("hide");
	var btnRequestBudget = js.JQuery("#btnRequestBudget");
	btnRequestBudget.removeClass("btn-primary");
	btnRequestBudget.removeClass("btn-success");
	btnRequestBudget.addClass("btn-danger");
	if(Client.documentLanguage == "en") btnRequestBudget.html("Error, please try again"); else btnRequestBudget.html("Erro, por favor tente de novo");
	var errorMsg = "";
	if(Client.documentLanguage == "en") errorMsg = "An error has occurred, please try again or contact us."; else errorMsg = "Ocorreu um erro, por favor tente de novo ou entre em contacto connosco.";
	utils_Utils.flashMessage(errorMsg,"danger");
};
Client.initTrackingForm = function() {
	var trackingFormOptions = { resetForm : true, success : Client.onTrackingSuccess, error : Client.onTrackingError};
	js.JQuery("#trackingForm").ajaxForm(trackingFormOptions);
};
Client.onTrackingSuccess = function(data,status) {
	if(data == null || data == "null") {
		var errorMsg = "";
		if(Client.documentLanguage == "en") errorMsg = "Invalid Code"; else errorMsg = "Código inválido";
		js.JQuery("#trackingTxtError").html(errorMsg);
		js.JQuery("#trackingTxtError").show();
		js.JQuery("#trackingPanel").hide();
	} else {
		js.JQuery("#trackingTxtError").hide();
		js.JQuery("#trackingPanel").show();
		if(typeof(data) == "string") data = JSON.parse(data);
		var location = data.location;
		var status1 = data.status;
		var s = Type.createEnum(OrderStatus,status1);
		if(s != null) status1 = Client.orderStatusToString(s); else status1 = "";
		if(location == null || location == "") location = "---";
		if(status1 == null || status1 == "") status1 = "---";
		js.JQuery("#txtLocation").html(location);
		js.JQuery("#txtStatus").html(status1);
	}
};
Client.orderStatusToString = function(status) {
	switch(status[1]) {
	case 3:
		if(Client.documentLanguage == "en") return "Canceled"; else return "Cancelada";
		break;
	case 2:
		if(Client.documentLanguage == "en") return "Finished"; else return "Terminada";
		break;
	case 0:
		if(Client.documentLanguage == "en") return "Open"; else return "Aberta";
		break;
	case 1:
		if(Client.documentLanguage == "en") return "Ongoing"; else return "Em curso";
		break;
	}
};
Client.onTrackingError = function(data,status) {
	utils_Utils.handleUfError(data,status);
	js.JQuery("#trackingPanel").hide();
};
Math.__name__ = true;
var Reflect = function() { };
Reflect.__name__ = true;
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		return null;
	}
};
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
};
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
};
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
var Structs = function() { };
Structs.__name__ = true;
var OrderStatus = { __ename__ : true, __constructs__ : ["Open","Started","Finished","Canceled"] };
OrderStatus.Open = ["Open",0];
OrderStatus.Open.toString = $estr;
OrderStatus.Open.__enum__ = OrderStatus;
OrderStatus.Started = ["Started",1];
OrderStatus.Started.toString = $estr;
OrderStatus.Started.__enum__ = OrderStatus;
OrderStatus.Finished = ["Finished",2];
OrderStatus.Finished.toString = $estr;
OrderStatus.Finished.__enum__ = OrderStatus;
OrderStatus.Canceled = ["Canceled",3];
OrderStatus.Canceled.toString = $estr;
OrderStatus.Canceled.__enum__ = OrderStatus;
var Type = function() { };
Type.__name__ = true;
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw new js__$Boot_HaxeError("No such constructor " + constr);
	if(Reflect.isFunction(f)) {
		if(params == null) throw new js__$Boot_HaxeError("Constructor " + constr + " need parameters");
		return Reflect.callMethod(e,f,params);
	}
	if(params != null && params.length != 0) throw new js__$Boot_HaxeError("Constructor " + constr + " does not need parameters");
	return f;
};
var haxe_IMap = function() { };
haxe_IMap.__name__ = true;
var haxe_ds_StringMap = function() {
	this.h = { };
};
haxe_ds_StringMap.__name__ = true;
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,__class__: haxe_ds_StringMap
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
js__$Boot_HaxeError.__name__ = true;
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
js_Boot.__name__ = true;
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__cast = function(o,t) {
	if(js_Boot.__instanceof(o,t)) return o; else throw new js__$Boot_HaxeError("Cannot cast " + Std.string(o) + " to " + Std.string(t));
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return (Function("return typeof " + name + " != \"undefined\" ? " + name + " : null"))();
};
var utils_Utils = function() { };
utils_Utils.__name__ = true;
utils_Utils.flashMessage = function(msg,type,align,allowDismiss) {
	if(allowDismiss == null) allowDismiss = true;
	if(align == null) align = "right";
	if(type != "info" && type != "danger" && type != "success" && type != null) type = "info";
	js.JQuery.bootstrapGrowl(msg,{ type : type, width : "auto", align : align, allow_dismiss : allowDismiss});
};
utils_Utils.createDOMFromString = function(s) {
	return js.JQuery.parseHTML(s);
};
utils_Utils.handleUfError = function(data,status) {
	var errMsg = utils_Utils.getUfrontError(data);
	utils_Utils.flashMessage(errMsg.title != null?errMsg.title:errMsg.msg,"danger");
	console.log(errMsg);
};
utils_Utils.getUfrontError = function(htmlError) {
	var jq = js.JQuery("<div>" + (htmlError.responseText + "</div>"));
	var title = jq.find(".error-data");
	var msg = jq.find(".error-message");
	var pos = jq.find(".error-pos");
	var stack = jq.find("code");
	return { title : title.text(), msg : msg.text(), pos : null, stack : null, raw : htmlError};
};
utils_Utils.centerDialog = function(modal) {
	modal.css("display","block");
	var dialog = modal.find(".modal-dialog");
	var content = modal.find(".modal-content");
	var height;
	height = modal.height() / 2 - dialog.height() / 2;
	dialog.css("margin-top",height);
};
utils_Utils.stringToFloat = function(str) {
	var f = parseFloat(str);
	if((function($this) {
		var $r;
		var f1;
		f1 = js_Boot.__cast(f , Float);
		$r = isNaN(f1);
		return $r;
	}(this))) return null; else return f;
};
utils_Utils.stringToJQuery = function(html) {
	return js.JQuery(js.JQuery.parseHTML(html));
};
utils_Utils.jcache = function(s,refresh) {
	if(refresh == null) refresh = false;
	var el = utils_Utils._JCache.get(s);
	if(el == null || refresh) {
		el = js.JQuery(s);
		if(el != null) utils_Utils._JCache.set(s,el);
	}
	return el;
};
String.prototype.__class__ = String;
String.__name__ = true;
Array.__name__ = true;
var Int = { __name__ : ["Int"]};
var Dynamic = { __name__ : ["Dynamic"]};
var Float = Number;
Float.__name__ = ["Float"];
var Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = { __name__ : ["Class"]};
var Enum = { };
var __map_reserved = {}
var q = window.jQuery;
var js = js || {}
js.JQuery = q;
js_Boot.__toStr = {}.toString;
utils_Utils._JCache = new haxe_ds_StringMap();
Client.main();
})(typeof console != "undefined" ? console : {log:function(){}});
