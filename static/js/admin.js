(function (console) { "use strict";
var $estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Admin = function() { };
Admin.__name__ = true;
Admin.main = function() {
	js.JQuery("document").ready(function(_) {
		Admin.init();
	});
};
Admin.init = function() {
	Admin.btnOrdersPage = js.JQuery("#btnOrdersPage");
	Admin.btnFormsPage = js.JQuery("#btnFormsPage");
	admin_OrdersScreen.init();
	admin_FormsScreen.init();
	Admin.btnOrdersPage.click(function(_) {
		admin_OrdersScreen.ordersPage.show(1);
		Admin.btnOrdersPage.parent().addClass("active");
		admin_FormsScreen.formsPage.hide(1);
		Admin.btnFormsPage.parent().removeClass("active");
	});
	Admin.btnFormsPage.click(function(_1) {
		admin_OrdersScreen.ordersPage.hide(1);
		Admin.btnOrdersPage.parent().removeClass("active");
		admin_FormsScreen.formsPage.show(1);
		Admin.btnFormsPage.parent().addClass("active");
	});
};
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
EReg.__name__ = true;
EReg.prototype = {
	match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,__class__: EReg
};
var HxOverrides = function() { };
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
Math.__name__ = true;
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
var Structs = function() { };
Structs.__name__ = true;
var OrderStatus = { __ename__ : true, __constructs__ : ["Open","Started","Finished","Canceled"] };
OrderStatus.Open = ["Open",0];
OrderStatus.Open.toString = $estr;
OrderStatus.Open.__enum__ = OrderStatus;
OrderStatus.Started = ["Started",1];
OrderStatus.Started.toString = $estr;
OrderStatus.Started.__enum__ = OrderStatus;
OrderStatus.Finished = ["Finished",2];
OrderStatus.Finished.toString = $estr;
OrderStatus.Finished.__enum__ = OrderStatus;
OrderStatus.Canceled = ["Canceled",3];
OrderStatus.Canceled.toString = $estr;
OrderStatus.Canceled.__enum__ = OrderStatus;
var admin_FormsScreen = function() { };
admin_FormsScreen.__name__ = true;
admin_FormsScreen.init = function() {
	admin_FormsScreen.formsPage = js.JQuery("#formsPage");
	admin_FormsScreen.btnRemoveForms = js.JQuery("#removeForms");
	admin_FormsScreen.btnRemoveForms.click(admin_FormsScreen.onRemoveForms);
	admin_FormsScreen.createTable();
	admin_FormsScreen.nameHolder = js.JQuery("#nameHolder");
	admin_FormsScreen.phoneHolder = js.JQuery("#phoneHolder");
	admin_FormsScreen.mailHolder = js.JQuery("#mailHolder");
	admin_FormsScreen.commentsHolder = js.JQuery("#commentsHolder");
};
admin_FormsScreen.createTable = function() {
	js.JQuery("#formsTable").bootstrapTable({ pagination : true, toolbar : "#formsToolbar", sortName : "id", sortOrder : "desc", url : "/admin/forms", showColumns : true, showRefresh : true, maintainSelected : false, columns : [{ field : "state", checkbox : true},{ title : "ID", field : "id", width : 30, sortable : true, visible : true},{ title : "Criado", field : "created", width : 70, formatter : utils_Table.dateFormatter, sortable : true},{ title : "IP", field : "ip", width : 70, sortable : true, visible : false},{ title : "Nome", field : "name", width : 200, sortable : true},{ title : "Email", field : "email", width : 100, sortable : true},{ title : "Tel", field : "phone", width : 70, sortable : true},{ title : "Origem", field : "orig", width : 100, sortable : true},{ title : "Destino", field : "dest", width : 100, sortable : true},{ title : "Inicio", field : "date", width : 70, formatter : utils_Table.dateFormatter, sortable : true},{ title : "Comentarios", field : "comments", visible : false}]}).on("all.bs.table",function(e,row) {
		console.log("EVT");
		console.log(e);
		admin_FormsScreen.countChecked();
	}).on("load-error.bs.table",function(data,status) {
		console.log(data);
		console.log(status);
	}).on("click-row.bs.table",function(row1,element) {
		var name = element.name;
		var comments = element.comments;
		var phone = element.phone;
		var mail = element.email;
		admin_FormsScreen.mailHolder.html(mail);
		admin_FormsScreen.nameHolder.html(name);
		admin_FormsScreen.phoneHolder.html(phone);
		admin_FormsScreen.commentsHolder.html(comments);
	});
};
admin_FormsScreen.countChecked = function() {
	var checked = js.JQuery("#formsTable").bootstrapTable("getSelections");
	if(checked.length > 0) admin_FormsScreen.btnRemoveForms.removeClass("disabled"); else admin_FormsScreen.btnRemoveForms.addClass("disabled");
};
admin_FormsScreen.onRemoveForms = function() {
	var checked = js.JQuery("#formsTable").bootstrapTable("getSelections");
	var ids;
	var _g = [];
	var _g1 = 0;
	while(_g1 < checked.length) {
		var row = checked[_g1];
		++_g1;
		_g.push(row.id);
	}
	ids = _g;
	js.JQuery.get("admin/forms/delete/" + JSON.stringify(ids),function(data) {
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	}).done(function(data2) {
		js.JQuery("#formsTable").bootstrapTable("refresh");
	});
};
var admin_OrdersScreen = function() { };
admin_OrdersScreen.__name__ = true;
admin_OrdersScreen.init = function() {
	admin_OrdersScreen.ordersPage = js.JQuery("#ordersPage");
	admin_OrdersScreen.btnAddOrder = js.JQuery("#addOrder");
	admin_OrdersScreen.btnRemoveOrder = js.JQuery("#removeOrder");
	admin_OrdersScreen.btnAddOrder.click(admin_OrdersScreen.onAddOrder);
	admin_OrdersScreen.btnRemoveOrder.click(admin_OrdersScreen.onRemoveOrders);
	admin_OrdersScreen.createTable();
};
admin_OrdersScreen.createTable = function() {
	js.JQuery("#ordersTable").bootstrapTable({ pagination : true, toolbar : "#ordersToolbar", search : true, url : "/admin/orders", sortName : "id", sortOrder : "desc", idField : "id", showColumns : true, showRefresh : true, maintainSelected : false, columns : [{ field : "state", checkbox : true},{ title : "ID", field : "id", width : 30, sortable : true, visible : false},{ title : "Ref#", field : "ref", width : 30, sortable : true, formatter : utils_Table.intFormatter, editable : { type : "text", url : "admin/orders/modify", name : "ref", error : function(r,v) {
		utils_Utils.handleUfError(r,"");
	}, params : function(params) {
		params.type = "int";
		return params;
	}}},{ title : "Cliente", field : "client", width : 170, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "client", error : function(r1,v1) {
		utils_Utils.handleUfError(r1,"");
	}}},{ title : "Agente", field : "agent", width : 170, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "agent", error : function(r2,v2) {
		utils_Utils.handleUfError(r2,"");
	}}},{ title : "Origem", field : "orig", width : 150, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "orig", error : function(r3,v3) {
		utils_Utils.handleUfError(r3,"");
	}}},{ title : "Destino", field : "dest", width : 150, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "dest", error : function(r4,v4) {
		utils_Utils.handleUfError(r4,"");
	}}},{ title : "Volume", field : "volume", width : 80, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "volume", error : function(r5,v5) {
		utils_Utils.handleUfError(r5,"");
	}}},{ title : "Preco", field : "budget", width : 80, sortable : true, formatter : utils_Table.currencyFormatter, editable : { type : "text", url : "admin/orders/modify", name : "budget", error : function(r6,v6) {
		utils_Utils.handleUfError(r6,"");
	}}},{ 'class' : "user-column", title : "Local", field : "location", width : 120, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "location", error : function(r7,v7) {
		utils_Utils.handleUfError(r7,"");
	}}},{ 'class' : "user-column", title : "Estado", field : "status", width : 100, sortable : true, editable : { type : "select", source : [{ value : OrderStatus.Open, text : Std.string(OrderStatus.Open)},{ value : OrderStatus.Started, text : Std.string(OrderStatus.Started)},{ value : OrderStatus.Finished, text : Std.string(OrderStatus.Finished)},{ value : OrderStatus.Canceled, text : Std.string(OrderStatus.Canceled)}], url : "admin/orders/modify", name : "status", error : function(r8,v8) {
		utils_Utils.handleUfError(r8,"");
	}}},{ 'class' : "user-column", title : "Codigo", field : "code", width : 150, visible : true}]}).on("load-error.bs.table",function(data,status) {
		console.log(data);
		console.log(status);
	}).on("all.bs.table",function(e,row) {
		admin_OrdersScreen.countOrdersChecked();
	});
};
admin_OrdersScreen.countOrdersChecked = function() {
	var checked = js.JQuery("#ordersTable").bootstrapTable("getSelections");
	if(checked.length > 0) admin_OrdersScreen.btnRemoveOrder.removeClass("disabled"); else admin_OrdersScreen.btnRemoveOrder.addClass("disabled");
};
admin_OrdersScreen.onRemoveOrders = function() {
	var checked = js.JQuery("#ordersTable").bootstrapTable("getSelections");
	var ids;
	var _g = [];
	var _g1 = 0;
	while(_g1 < checked.length) {
		var row = checked[_g1];
		++_g1;
		_g.push(row.id);
	}
	ids = _g;
	js.JQuery.get("admin/orders/delete/" + JSON.stringify(ids),function(data) {
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	}).done(function(data2) {
		js.JQuery("#ordersTable").bootstrapTable("refresh");
	});
};
admin_OrdersScreen.onAddOrder = function() {
	var args = { client : null, agent : null, orig : null, dest : null, volume : null, location : null, budget : 0};
	js.JQuery.post("admin/orders/create",args,function(data) {
		js.JQuery("#ordersTable").bootstrapTable("refresh");
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	});
};
var haxe_IMap = function() { };
haxe_IMap.__name__ = true;
var haxe_ds_StringMap = function() {
	this.h = { };
};
haxe_ds_StringMap.__name__ = true;
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,__class__: haxe_ds_StringMap
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
js__$Boot_HaxeError.__name__ = true;
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
js_Boot.__name__ = true;
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__cast = function(o,t) {
	if(js_Boot.__instanceof(o,t)) return o; else throw new js__$Boot_HaxeError("Cannot cast " + Std.string(o) + " to " + Std.string(t));
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return (Function("return typeof " + name + " != \"undefined\" ? " + name + " : null"))();
};
var utils_Table = function() { };
utils_Table.__name__ = true;
utils_Table.dateFormatter = function(value,row) {
	return value.split(" ")[0];
};
utils_Table.currencyFormatter = function(value,row) {
	if(!utils_Table.startsWithNumber.match(value)) value = value.substring(1);
	var intValue = utils_Utils.stringToFloat(value);
	if(intValue == null) intValue = 0;
	if(intValue == null) value = "null"; else value = "" + intValue;
	return "€ " + value;
};
utils_Table.intFormatter = function(value,row) {
	if(Std.parseInt(value) == null) return 0;
	return Std.parseInt(value);
};
var utils_Utils = function() { };
utils_Utils.__name__ = true;
utils_Utils.flashMessage = function(msg,type,align,allowDismiss) {
	if(allowDismiss == null) allowDismiss = true;
	if(align == null) align = "right";
	if(type != "info" && type != "danger" && type != "success" && type != null) type = "info";
	js.JQuery.bootstrapGrowl(msg,{ type : type, width : "auto", align : align, allow_dismiss : allowDismiss});
};
utils_Utils.createDOMFromString = function(s) {
	return js.JQuery.parseHTML(s);
};
utils_Utils.handleUfError = function(data,status) {
	var errMsg = utils_Utils.getUfrontError(data);
	utils_Utils.flashMessage(errMsg.title != null?errMsg.title:errMsg.msg,"danger");
	console.log(errMsg);
};
utils_Utils.getUfrontError = function(htmlError) {
	var jq = js.JQuery("<div>" + (htmlError.responseText + "</div>"));
	var title = jq.find(".error-data");
	var msg = jq.find(".error-message");
	var pos = jq.find(".error-pos");
	var stack = jq.find("code");
	return { title : title.text(), msg : msg.text(), pos : null, stack : null, raw : htmlError};
};
utils_Utils.centerDialog = function(modal) {
	modal.css("display","block");
	var dialog = modal.find(".modal-dialog");
	var content = modal.find(".modal-content");
	var height;
	height = modal.height() / 2 - dialog.height() / 2;
	dialog.css("margin-top",height);
};
utils_Utils.stringToFloat = function(str) {
	var f = parseFloat(str);
	if((function($this) {
		var $r;
		var f1;
		f1 = js_Boot.__cast(f , Float);
		$r = isNaN(f1);
		return $r;
	}(this))) return null; else return f;
};
utils_Utils.stringToJQuery = function(html) {
	return js.JQuery(js.JQuery.parseHTML(html));
};
utils_Utils.jcache = function(s,refresh) {
	if(refresh == null) refresh = false;
	var el = utils_Utils._JCache.get(s);
	if(el == null || refresh) {
		el = js.JQuery(s);
		if(el != null) utils_Utils._JCache.set(s,el);
	}
	return el;
};
String.prototype.__class__ = String;
String.__name__ = true;
Array.__name__ = true;
var Int = { __name__ : ["Int"]};
var Dynamic = { __name__ : ["Dynamic"]};
var Float = Number;
Float.__name__ = ["Float"];
var Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = { __name__ : ["Class"]};
var Enum = { };
var __map_reserved = {}
var q = window.jQuery;
var js = js || {}
js.JQuery = q;
js_Boot.__toStr = {}.toString;
utils_Table.startsWithNumber = new EReg("^[0-9-.]","");
utils_Utils._JCache = new haxe_ds_StringMap();
Admin.main();
})(typeof console != "undefined" ? console : {log:function(){}});
