(function (console, $hx_exports) { "use strict";
$hx_exports.promhx = $hx_exports.promhx || {};
var $hxClasses = {},$estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Admin = function() { };
$hxClasses["Admin"] = Admin;
Admin.__name__ = ["Admin"];
Admin.main = function() {
	js.JQuery("document").ready(function(_) {
		Admin.init();
	});
};
Admin.init = function() {
	Admin.btnOrdersPage = js.JQuery("#btnOrdersPage");
	Admin.btnFormsPage = js.JQuery("#btnFormsPage");
	admin_OrdersScreen.init();
	admin_FormsScreen.init();
	Admin.btnOrdersPage.click(function(_) {
		admin_OrdersScreen.ordersPage.show(1);
		Admin.btnOrdersPage.parent().addClass("active");
		admin_FormsScreen.formsPage.hide(1);
		Admin.btnFormsPage.parent().removeClass("active");
	});
	Admin.btnFormsPage.click(function(_1) {
		admin_OrdersScreen.ordersPage.hide(1);
		Admin.btnOrdersPage.parent().removeClass("active");
		admin_FormsScreen.formsPage.show(1);
		Admin.btnFormsPage.parent().addClass("active");
	});
};
var Client = function() { };
$hxClasses["Client"] = Client;
Client.__name__ = ["Client"];
Client.main = function() {
	js.JQuery("document").ready(function(_) {
		Client.init();
	});
};
Client.init = function() {
	js.JQuery(".carousel").carousel({ interval : 5000});
	Client.documentLanguage = js.JQuery("html").attr("lang").toLowerCase();
	Client.initBudgetForm();
	Client.initTrackingForm();
};
Client.initBudgetForm = function() {
	var beforeBudgetSubmit = function() {
		js.JQuery("#btnBFSubmit").attr("disabled",true).html("A Enviar...");
	};
	var budgetFormOptions = { resetForm : true, beforeSubmit : beforeBudgetSubmit, success : Client.onBudgetSubmited, error : Client.onBudgetError, timeout : 3000};
	js.JQuery("#budgetForm").ajaxForm(budgetFormOptions);
};
Client.onBudgetSubmited = function(data,status) {
	haxe_Log.trace(data,{ fileName : "Client.hx", lineNumber : 46, className : "Client", methodName : "onBudgetSubmited"});
	js.JQuery("#btnBFSubmit").attr("disabled",false).html("Enviar Pedido");
	js.JQuery("#budgetDialog").modal("hide");
	var errorMsg = "";
	var btnRequestBudget = js.JQuery("#btnRequestBudget");
	btnRequestBudget.removeClass("btn-primary");
	btnRequestBudget.removeClass("btn-danger");
	btnRequestBudget.addClass("btn-success");
	if(Client.documentLanguage == "en") btnRequestBudget.html("Request Sent"); else btnRequestBudget.html("Pedido Enviado");
	if(Client.documentLanguage == "en") errorMsg = "Thanks, your request has been sent and will be answered soon."; else errorMsg = "Obrigado, o seu pedido foi enviado e será respondido em breve.";
	utils_Utils.flashMessage(errorMsg,"success");
};
Client.onBudgetError = function(data) {
	haxe_Log.trace(data,{ fileName : "Client.hx", lineNumber : 67, className : "Client", methodName : "onBudgetError"});
	js.JQuery("#btnBFSubmit").attr("disabled",false).html("Enviar Pedido");
	js.JQuery("#budgetDialog").modal("hide");
	var btnRequestBudget = js.JQuery("#btnRequestBudget");
	btnRequestBudget.removeClass("btn-primary");
	btnRequestBudget.removeClass("btn-success");
	btnRequestBudget.addClass("btn-danger");
	if(Client.documentLanguage == "en") btnRequestBudget.html("Error, please try again"); else btnRequestBudget.html("Erro, por favor tente de novo");
	var errorMsg = "";
	if(Client.documentLanguage == "en") errorMsg = "An error has occurred, please try again or contact us."; else errorMsg = "Ocorreu um erro, por favor tente de novo ou entre em contacto connosco.";
	utils_Utils.flashMessage(errorMsg,"danger");
};
Client.initTrackingForm = function() {
	var trackingFormOptions = { resetForm : true, success : Client.onTrackingSuccess, error : Client.onTrackingError};
	js.JQuery("#trackingForm").ajaxForm(trackingFormOptions);
};
Client.onTrackingSuccess = function(data,status) {
	if(data == null || data == "null") {
		var errorMsg = "";
		if(Client.documentLanguage == "en") errorMsg = "Invalid Code"; else errorMsg = "Código inválido";
		js.JQuery("#trackingTxtError").html(errorMsg);
		js.JQuery("#trackingTxtError").show();
		js.JQuery("#trackingPanel").hide();
	} else {
		js.JQuery("#trackingTxtError").hide();
		js.JQuery("#trackingPanel").show();
		if(typeof(data) == "string") data = JSON.parse(data);
		var location = data.location;
		var status1 = data.status;
		var s = Type.createEnum(OrderStatus,status1);
		if(s != null) status1 = Client.orderStatusToString(s); else status1 = "";
		if(location == null || location == "") location = "---";
		if(status1 == null || status1 == "") status1 = "---";
		js.JQuery("#txtLocation").html(location);
		js.JQuery("#txtStatus").html(status1);
	}
};
Client.orderStatusToString = function(status) {
	switch(status[1]) {
	case 3:
		if(Client.documentLanguage == "en") return "Canceled"; else return "Cancelada";
		break;
	case 2:
		if(Client.documentLanguage == "en") return "Finished"; else return "Terminada";
		break;
	case 0:
		if(Client.documentLanguage == "en") return "Open"; else return "Aberta";
		break;
	case 1:
		if(Client.documentLanguage == "en") return "Ongoing"; else return "Em curso";
		break;
	}
};
Client.onTrackingError = function(data,status) {
	utils_Utils.handleUfError(data,status);
	js.JQuery("#trackingPanel").hide();
};
var CompileTime = function() { };
$hxClasses["CompileTime"] = CompileTime;
CompileTime.__name__ = ["CompileTime"];
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
$hxClasses["EReg"] = EReg;
EReg.__name__ = ["EReg"];
EReg.prototype = {
	match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,matched: function(n) {
		if(this.r.m != null && n >= 0 && n < this.r.m.length) return this.r.m[n]; else throw new js__$Boot_HaxeError("EReg::matched");
	}
	,__class__: EReg
};
var HxOverrides = function() { };
$hxClasses["HxOverrides"] = HxOverrides;
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
var Lambda = function() { };
$hxClasses["Lambda"] = Lambda;
Lambda.__name__ = ["Lambda"];
Lambda.exists = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		if(f(x)) return true;
	}
	return false;
};
Lambda.iter = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		f(x);
	}
};
Lambda.concat = function(a,b) {
	var l = new List();
	var $it0 = $iterator(a)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		l.add(x);
	}
	var $it1 = $iterator(b)();
	while( $it1.hasNext() ) {
		var x1 = $it1.next();
		l.add(x1);
	}
	return l;
};
var List = function() {
	this.length = 0;
};
$hxClasses["List"] = List;
List.__name__ = ["List"];
List.prototype = {
	add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,push: function(item) {
		var x = [item,this.h];
		this.h = x;
		if(this.q == null) this.q = x;
		this.length++;
	}
	,first: function() {
		if(this.h == null) return null; else return this.h[0];
	}
	,pop: function() {
		if(this.h == null) return null;
		var x = this.h[0];
		this.h = this.h[1];
		if(this.h == null) this.q = null;
		this.length--;
		return x;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,iterator: function() {
		return new _$List_ListIterator(this.h);
	}
	,filter: function(f) {
		var l2 = new List();
		var l = this.h;
		while(l != null) {
			var v = l[0];
			l = l[1];
			if(f(v)) l2.add(v);
		}
		return l2;
	}
	,__class__: List
};
var _$List_ListIterator = function(head) {
	this.head = head;
	this.val = null;
};
$hxClasses["_List.ListIterator"] = _$List_ListIterator;
_$List_ListIterator.__name__ = ["_List","ListIterator"];
_$List_ListIterator.prototype = {
	hasNext: function() {
		return this.head != null;
	}
	,next: function() {
		this.val = this.head[0];
		this.head = this.head[1];
		return this.val;
	}
	,__class__: _$List_ListIterator
};
Math.__name__ = ["Math"];
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = ["Reflect"];
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		haxe_CallStack.lastException = e;
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		return null;
	}
};
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
};
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = ["Std"];
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
var StringTools = function() { };
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = ["StringTools"];
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && HxOverrides.substr(s,0,start.length) == start;
};
StringTools.endsWith = function(s,end) {
	var elen = end.length;
	var slen = s.length;
	return slen >= elen && HxOverrides.substr(s,slen - elen,elen) == end;
};
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c > 8 && c < 14 || c == 32;
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
};
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) r++;
	if(r > 0) return HxOverrides.substr(s,0,l - r); else return s;
};
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
};
StringTools.lpad = function(s,c,l) {
	if(c.length <= 0) return s;
	while(s.length < l) s = c + s;
	return s;
};
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
};
var Structs = function() { };
$hxClasses["Structs"] = Structs;
Structs.__name__ = ["Structs"];
var OrderStatus = { __ename__ : true, __constructs__ : ["Open","Started","Finished","Canceled"] };
OrderStatus.Open = ["Open",0];
OrderStatus.Open.toString = $estr;
OrderStatus.Open.__enum__ = OrderStatus;
OrderStatus.Started = ["Started",1];
OrderStatus.Started.toString = $estr;
OrderStatus.Started.__enum__ = OrderStatus;
OrderStatus.Finished = ["Finished",2];
OrderStatus.Finished.toString = $estr;
OrderStatus.Finished.__enum__ = OrderStatus;
OrderStatus.Canceled = ["Canceled",3];
OrderStatus.Canceled.toString = $estr;
OrderStatus.Canceled.__enum__ = OrderStatus;
var buddy_BuddySuite = function() {
	this.suites = new List();
	this.befores = new List();
	this.afters = new List();
	this.suiteStack = new List();
	this.timeoutMs = 5000;
};
$hxClasses["buddy.BuddySuite"] = buddy_BuddySuite;
buddy_BuddySuite.__name__ = ["buddy","BuddySuite"];
buddy_BuddySuite.prototype = {
	describe: function(name,addSpecs) {
		this.addSuite(new buddy_Suite(name,this),addSpecs);
	}
	,xdescribe: function(name,addSpecs) {
	}
	,before: function(init) {
		this.syncBefore(init,true);
	}
	,after: function(deinit) {
		this.syncAfter(deinit,true);
	}
	,it: function(desc,test) {
		this.syncIt(desc,test,true);
	}
	,xit: function(desc,test) {
		this.syncXit(desc,test,true);
	}
	,fail: function(desc) {
		if(desc == null) desc = "Manually";
	}
	,failSync: function(test,desc,p) {
		if(desc == null) desc = "Manually";
		var stackItem = [haxe_StackItem.FilePos(null,p.fileName,p.lineNumber)];
		test(false,Std.string(desc),stackItem);
	}
	,addSuite: function(suite,addSpecs) {
		if(this.suiteStack.isEmpty()) this.suites.add(suite); else {
			var current = this.suiteStack.first();
			suite.parent = current;
			current.steps.add(buddy_TestStep.TSuite(suite));
		}
		if(buddy_BuddySuite.includeMode && !suite.include) {
			suite.steps = suite.steps.filter(function(step) {
				switch(step[1]) {
				case 1:
					var s = step[2];
					return s.include;
				default:
					return true;
				}
			});
			if(suite.steps.length > 0 || suite.parent != null && suite.parent.include) suite.include = true;
		}
		this.suiteStack.push(suite);
		addSpecs();
		this.suiteStack.pop();
	}
	,describeInclude: function(name,addSpecs) {
		buddy_BuddySuite.includeMode = true;
		var suite = new buddy_Suite(name,this);
		suite.include = true;
		this.addSuite(suite,addSpecs);
	}
	,itInclude: function(desc,test) {
		buddy_BuddySuite.includeMode = true;
		this.syncIt(desc,test,true,true);
	}
	,syncItInclude: function(desc,test) {
		buddy_BuddySuite.includeMode = true;
		this.syncIt(desc,test,false,true);
	}
	,beforeDescribe: function(init) {
		this.syncBeforeDescribe(init,true);
	}
	,afterDescribe: function(init) {
		this.syncAfterDescribe(init,true);
	}
	,syncBeforeDescribe: function(init,async) {
		if(async == null) async = false;
		this.befores.add(new buddy_BeforeAfter(init,async));
	}
	,syncAfterDescribe: function(init,async) {
		if(async == null) async = false;
		this.afters.add(new buddy_BeforeAfter(init,async));
	}
	,syncBefore: function(init,async) {
		if(async == null) async = false;
		this.suiteStack.first().before.add(new buddy_BeforeAfter(init,async));
	}
	,syncAfter: function(deinit,async) {
		if(async == null) async = false;
		this.suiteStack.first().after.add(new buddy_BeforeAfter(deinit,async));
	}
	,syncIt: function(desc,test,async,include) {
		if(include == null) include = false;
		if(async == null) async = false;
		var suite = this.suiteStack.first();
		var spec = new buddy_Spec(suite,desc,test,async);
		spec.include = include;
		suite.steps.add(buddy_TestStep.TSpec(spec));
	}
	,syncXit: function(desc,test,async) {
		if(async == null) async = false;
		var suite = this.suiteStack.first();
		var spec = new buddy_Spec(suite,desc,test,async,true);
		suite.steps.add(buddy_TestStep.TSpec(spec));
	}
	,__class__: buddy_BuddySuite
};
var buddy_Buddy = function() { };
$hxClasses["buddy.Buddy"] = buddy_Buddy;
buddy_Buddy.__name__ = ["buddy","Buddy"];
var TestClient = function() {
	this.enHomePage = "<!DOCTYPE html>\n\n<html lang=\"en\">\n\n\n<head>\n\n\t<meta charset=\"utf-8\">\n\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\t<meta name=\"description\" content=\"\">\n\t<meta name=\"author\" content=\"\">\n\n\t<title>Invictus Relocation</title>\n\t\n\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">\n\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css\">\n\t<!-- build:css public/css/combined.css -->\n\t<link rel=\"stylesheet\" href=\"public/css/main.css\" >\n\t<link rel=\"stylesheet\" href=\"public/css/lightbox.css\"  />\n\t<!-- endbuild -->\n\t<link rel=\"stylesheet\" href=\"public/font-awesome/css/font-awesome.min.css\">\n\n\t<script>\n\t\t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n\t\t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n\t\tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n\t\t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n\t\t\n\t\tga('create', 'UA-55271438-3', 'auto');\n\t\tga('send', 'pageview');\n\t</script>\n\t\t\n</head>\n\n<body>\n\n\t<div id=\"fb-root\"></div>\n\t<script>(function(d, s, id) {\n\tvar js, fjs = d.getElementsByTagName(s)[0];\n\tif (d.getElementById(id)) return;\n\tjs = d.createElement(s); js.id = id;\n\tjs.src = \"//connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.0\";\n\tfjs.parentNode.insertBefore(js, fjs);\n\t}(document, 'script', 'facebook-jssdk'));</script>\n\n\t\n\t<nav class=\"navbar navbar-default\" role=\"navigation\">\n\t\t<div class=\"container\">\n\n\t\t\t<div class=\"navbar-header\">\n\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t</button>\n\t\t\t\t<a class=\"navbar-brand\" href=\"#\">Invictus Relocation</a>\n\t\t\t</div>\n\n\t\t\t<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\t\t\t\t<ul class=\"nav navbar-nav\">\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#about\">About Us</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#services\">Services</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#facilities\">Facilities</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#materials\">Materiais</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#contacts\">Contacts</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\n\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#budget\">Budget</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#tracking\">Tracking</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"dropdown\">\n\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><span class=\"label label-success\">\n\t\t\t\t\t\t\tEnglish\n\t\t\t\t\t\t</span><span class=\"caret\"></span></a>\n\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">\n\t\t\t\t\t\t\t<li><a href=\"?lang=pt\" class=\"text-right\">Português (PT)</a></li>\n\t\t\t\t\t\t\t<li><a href=\"?lang=en\" class=\"text-right\">English (EN)</a></li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t</li>\n\t\t\t\t\t\n\t\t\t\t</ul>\n\n\t\t\t</div>\n\n\t\t</div>\n\t</nav>\n\n\t<header class=\"business-header\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t<h1 class=\"tagline\">Your Relocation</h1>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\n\t<!-- Heading Row -->\n\t<div class=\"container\" style=\"margin-top:40px\">\n\n\t\t<div class=\"row\" id=\"about\">\n\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t<h1 class=\"text-center\" style=\"margin-bottom:40px\">Welcome to Invictus Relocation</h1>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"col-sm-6 col-sm-push-3 text-center\" style=\"margin-bottom:10px\">\n\t\t\t\t<img class=\"img-responsive img-rounded\" src=\"public/img/invictus-logo.png\" alt=\"\" style=\"margin-top:10px;margin-bottom:20px\">\n\t\t\t\t<p> Invictus Relocation is the new choice of relocation services, we provide services with highest quality in the field.\n\t\t\t\t\t<br>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!--SERVIÇOS -->\n\t<div class=\"container\">\n\t\t<hr>\n\t</div>\n\n\t<div class=\"container\" style=\"margin-bottom:40px\" id=\"services\">\n\t<div class=\"row w-margin\">\n\t\t<div class=\"col-lg-12\">\n\t\t\t<h1 class=\"text-center\" style=\"margin-bottom:40px;margin-top:-20px\">\n\t\t\t\tOur Services\n\t\t\t</h1>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t\t<i class=\"fa fa-truck fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t\t<h4 class=\"service-title\">International <br>Moves</h4>\n\t\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>We make the international moving process a simple and convenient task.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogInternational\" data-title=\"Mudanças Internacionais\" data-content=\"testInfo\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-flag fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">National <br>Moves</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Invictus offers several solutions for change within the country.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogNational\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-compass fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">Logistics</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>In Invictus we put at their disposal all the logistics linked to the transport of all your goods.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogLogistics\">Know More</a>\n\t\t</div>\n\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-plane fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">Air Sea and<br>Road Transportation</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Different means of transport are available.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogTransportation\">Know More</a>\n\t\t</div>\n\t</div>\n\n\t<!-- /.row -->\n\t<div class=\"row w-margin\" style=\"padding-top:20px\">\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-archive fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Storage</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Safety and quality are things we look for in storage equipment.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogStorage\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-fax fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Office <br>moves</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Transfer your office to another location quickly and smoothly.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogOffice\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-paint-brush fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Art Transportation</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Transport of works of art and other fragile goods is made with care.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogArtworks\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-plus fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Extra Services</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>A wide range of supplementary services is also available.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogExtras\">Know More</a>\n\t\t</div>\n\t</div>\n\t\n\t<!-- /.row -->\n\t<div class=\"row w-margin\" style=\"padding-top:20px\">\n\t\t<div class=\"col-sm-3\">\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-paw fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Pet Moving</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>We handle the transport of your pets with the utmost care.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogPets\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-home fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Relocation</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Invicuts offers a complete relocation service.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogRelocation\">Know More</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t</div>\n\t</div>\n</div>\n\t\n\t<!-- /.SERVICES -->\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\t\n\t<header class=\"background-img1\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"facilities\">\n\t\t\t<div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">Our Facilities</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<p>Our facilities are well located and have the equipment and facilities necessary to meet diverse logistics operations.</p>\n\t\t\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogFacilities\">Know More</a>\n\t\t\t\t</div>\n\t\t\t</div><!-- \n   \t\t\t\n\t\t--><div class=\"col-sm-2 vcenter\"></div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/office.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/office-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/warehouse2.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse2-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t\t<a href=\"public/img/gallery/warehouse.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\t<header class=\"background-img2\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"materials\">\n\t\t\t<div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/materials.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/materials-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t\t<a href=\"public/img/gallery/truck.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/truck-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/warehouse3.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse3-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-2 vcenter\">\n\t\t   </div><!--\n\t\t--><div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">Our Materials</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<p>The materials used in the transport and storage of orders must be appropriate, for this we have a wide range of solutions.</p>\n\t\t\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogMaterials\">Know More</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\n\t<header class=\"background-img3\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"contacts\">\n\t\t\t<div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">Contacts</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<address>\n\t\t\t\t\t\t\tEdificio Invictus - Armazém C<br>\n\t\t\t\t\t\t\tAvenida Santa Isabel nº 4 e 8<br>\n\t\t\t\t\t\t\t2635-047 Cabra Figa<br>\n\t\t\t\t\t\t\tPortugal<br><br>\n\t\t\t\t\t\t\t<strong>Tel:</strong> (+351) 219 259 580<br>\n\t\t\t\t\t\t\t<strong>Fax:</strong> (+351) 219 259 582<br>\n\t\t\t\t\t\t\t<strong>Email:</strong> <a href=\"mailto:enquiries@invictus-relo.com\" target=\"_top\">enquiries@invictus-relo.com</a>\n\t\t\t\t\t\t</address>\n\t\t\t\t</div>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-2 vcenter\"></div><!--\n\t\t--><div class=\"col-sm-6 vcenter google-maps\">\n\t\t\t\t<iframe frameborder=\"0\" style=\"border:0\" src=\"https://www.google.com/maps/embed/v1/streetview?location=38.746464,-9.342258&heading=90&pitch=3&key=AIzaSyB9eaq75nzfAMcM8uF-NGq1AikRakgO0oY\"></iframe>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\n\t<!-- ORCAMENTO -->\n\t<header>\n\t\t<div class=\"\">\n\t\t\t<div class=\"budget-holder jumbotron\" id=\"budget\">\n\t\t\t\t<div class=\"row w-margin\">\n\t\t\t\t\t<div class=\"col-lg-12 text-center\">\n\t\t\t\t\t\t<h3>Fill our form and receive a custom budget for free.</h3>\n\t\t\t\t\t\t<span id=\"btnRequestBudget\" class=\"btn btn-lg btn-primary\" href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#budgetDialog\" style=\"margin-top:20px\">\n\t\t\t\t\t\tRequest Budget\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\t\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\t\n\t<!-- REDES SOCIAIS -->\n\t<header class=\"background-img2\">\n\t\t<div class=\"container\" style=\"padding-bottom:20px;padding-top:20px\" id=\"social\">\n\t\t\t<div class=\"col-xs-6 vcenter\">\n\t\t\t\t<div style=\"padding-right:20px; padding-top:20px; padding-bottom:20px\">\n\t\t\t\t<div class=\"fb-like-box\" data-href=\"https://www.facebook.com/InvictusRelocationServices\" data-colorscheme=\"light\" data-show-faces=\"false\" data-header=\"false\" data-width=\"auto\" data-stream=\"true\" data-show-border=\"true\"></div>\n\t\t\t\t</div>\n\t\t\t</div><!--\n\t\t\t--><div class=\"col-xs-6 vcenter\">\n\t\t\t\t<h2 class=\"feature-header\">Find us also on social-networks.</h2>\n\t\t\t\t<div class=\"text-center\">\n\t\t\t\t\t<!--p>[Facebook][Twitter]</p-->\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\t\n\t<!-- ORDER TRACKING -->\n\t<div class=\"container\">\n\t\t<div class=\"row jumbotron tracking-container\" id=\"tracking\">\n\t\t\t<div class=\"container\">\n\t\t\t<div class=\"col-xs-6 vcenter\">\n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<h2 class=\"feature-header white-text\">Move Tracking</h2>\n\t\t\t\t</div>\n\t\t\t\t<h4 class=\"white-text\" style=\"line-height:35px;\">\n\t\t\t\tIf you already have an order, enter your <strong>tracking code</strong> here to check its status and location.\t\n\t\t\t\t</h4>\n\t\t\t</div><!-- \n\t\t\t<--><div class=\"col-xs-6 vcenter text-center\">\n\t\t\t\t<form id=\"trackingForm\" class=\"form-inline\" method=\"post\" action=\"orderTracking\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t<div class=\"input-group-addon\">\n\t\t\t\t\t\t\t\t<span><i class=\"fa fa-info-circle fa-lg\"></i></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" id=\"code\" name=\"code\" placeholder=\"Tracking Code\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-success btn-lg\">Find</button>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\t\n\t\t\t\t<div id=\"trackingTxtError\" style=\"color:#FF0000;display:none\">Error</div>\n\t\t\t\t<div id=\"trackingPanel\" class=\"row\" style=\"margin-top:20px;display:none\">\n\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t<h4><small class=\"white-text\">State</small></h4>\n\t\t\t\t\t\t<div id=\"txtStatus\" class=\"well well-sm\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t<h4><small class=\"white-text\">Local</small></h4>\n\t\t\t\t\t\t<div id=\"txtLocation\" class=\"well well-sm\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<!--div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\t\n\t\n\t\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div-->\n\t\n\t<!-- CERTIFICADOS -->\n\t<div class=\"container\" style=\"padding-top:20px\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-xs-12 vcenter text-center\">\n\t\t\t\t<h4>Certified by:</h4>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-xs-4 col-xs-offset-4\">\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-dod.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-iam.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-imc.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t\n\t<div class=\"container\">\n\t<div class=\"row\" id='gallery'>\n\t\t<div class=\"col-lg-12\">\n\t\t\t<h1 class=\"page-header text-center\">Gallery</h1>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/associations.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/associations-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/boxes.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/boxes-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/cargo.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/cargo-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/morecargo.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/morecargo-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/office.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/office-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/viatura2.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/viatura2-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t</div>\n</div>\n\t\n\t<hr>\n\n\t<!-- Footer -->\n\t<footer>\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-12 text-center\">\n\t\t\t\t\t<h4><small>Copyright &copy; InvictusRelo - TiagoLr 2015</small></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</footer>\n\n\t<!-- /.container -->\n\n\t<div id=\"budgetDialog\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t<div class=\"modal-dialog\">\n\t\t<div class=\"modal-content\">\n\t\t\t<div class=\"modal-header\">\n\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t</button>\n\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Budget Request</h4>\n\t\t\t</div>\n\t\t\t<div class=\"modal-body\">\n\t\t\t\t<form id='budgetForm' class=\"form-horizontal\" method=\"post\" action=\"requestBudget\">\n\t\t\t\t\t<fieldset>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"name\" class=\"col-sm-3 control-label\">Name*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Joe Steven\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"email\" class=\"col-sm-3 control-label\">Email*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"example@email.com\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"phone\" class=\"col-sm-3 control-label\">Phone*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" placeholder=\"+351 808 910 100\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"origin\" class=\"col-sm-3 control-label\">Origin*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"origin\" name=\"origin\" placeholder=\"Lisbon, Portugal\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"destination\" class=\"col-sm-3 control-label\">Destiny*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"destination\" name=\"destination\" placeholder=\"Paris, France\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"date\" class=\"col-sm-3 control-label\">Move Date*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"date\" class=\"form-control\" id=\"date\" name=\"date\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"comments\" class=\"col-sm-3 control-label\">Comments</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" id=\"comments\" name=\"comments\" placeholder=\"Put here comments or additional information in relation to your request.\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"hidden\" name=\"protection\" id=\"protection\">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"dialog-error-msg\"></div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\"></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<!--\t\t\t\t\t\t\t\t\t<div class=\"checkbox\">-->\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input id=\"human\" name=\"human\" type=\"checkbox\" value=\"true\"> I confirm that the data entered is correct.\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<!--\t\t\t\t\t\t\t\t\t</div>-->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-12 text-right\">\n\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n\t\t\t\t\t\t\t\t<button id=\"btnBFSubmit\" class=\"btn btn-primary\">Send Request</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n</div>\n\t\n\t<!-- INFO DIALOGS -->\n\t\n\t<div id=\"dialogInternational\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">International Moves</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Choose a place to go, Invictus Relocation takes care of the rest. </p>\r\n<p> Choosing the right method of transport is a vital part of the international change process. As such, Invictus will elect the more appropriate means of transport according to each customer's needs. By air, sea or land your cargo is safe. </p>\r\n<p> <strong> We plan your move: </strong> </p>\r\n<p> Invictus will appoint a marketing executive to make a visit to the customer's home, to make a survey of customer needs, check the volume to carry and determine the type of packaging materials to be used to ensure a quality service. </p>\r\n<p> <strong> Our services include: </strong> </p>\r\n<ul>\r\n<li> Survey with the client</li>\r\n<li> Volume Verification </li>\r\n<li> Access Verification / conditions for loading / unloading </li>\r\n<li> Protection of the house floor and interior</li>\r\n<li> Export Packing </li>\r\n<li> Load Inventory Carrying </li>\r\n<li> Labeling each volume </li>\r\n</ul>\r\n<p> <strong> Delivery Destination: </strong> </p>\r\n<p>\r\nInvictus will inform you about all the documentation required for customs in the destination country. Through our worldwide network of agents, we are able to deliver your belongings in any country in the world.\r\n</p>\r\n\r\n\r\n<img class=\"img-responsive img-portfolio img-rounded\" src=\"public/img/worldmap.jpg\" alt=\"\">\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogNational\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">National Moves</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Choose a place to go, the Invictus Relocation takes care of the rest. </p>\r\n<p> <strong> We plan your move: </strong> </p>\r\n<p> Invictus will appoint a marketing executive to make a visit to the customer's home, to make a survey of customer needs, check the volume to carry and determine the type of packaging materials to be used to ensure a quality service. </p>\r\n<p> <strong> Our services include: </strong> </p>\r\n<ul>\r\n<li> Survey with the client </li>\r\n<li> Volume Verification </li>\r\n<li> Access Verification / conditions for loading / unloading </li>\r\n<li> Protection of the house floor and interior </li>\r\n<li> Packing </li>\r\n<li> Load Inventory Carrying </li>\r\n<li> Labeling each volume </li>\r\n</ul>\r\n\r\n<p> <strong> Delivery Destination: </strong> </p>\r\n<p> Delivery will be effected, unpacked, and furniture will be installed if requested by the customer. </p>\r\n\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogExtras\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Extra Services</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> As each change is unique, in Invictus we offer a wide range of extra services to meet all your personal needs: </p>\r\n<ul>\r\n<li> Transport pianos / tuning </li>\r\n<li> Unwanted goods Removal </li>\r\n<li> Donations to charitable organizations </li>\r\n<li> Transport vehicles </li>\r\n<li> Cleaning Services </li>\r\n<li> Carpenter service, plumber, electrician </li>\r\n<li> Furniture Restoration </li>\r\n</ul>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogLogistics\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Logistics</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>The Invictus offers you several options of logistics: </p>\r\n\r\n<ul>\r\n<li> By Road: For intra European services</li>\r\n<li>By Air:. For faster delivery </li>\r\n<li>By Sea: For International Services </li>\r\n<li>LCL (Groupage Services):. Use Partial containers for smaller expeditions</li>\r\n<li>FCL (Full Container Load):. Only use containers</li>\r\n</ul>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogTransportation\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Transportation</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nThe method of transport is a vital part of the change process. Invictus deals with the client what is the most appropriate shipping method.\r\nBy air, sea or land their cargo is safe.\r\n</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogStorage\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Storage</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Invictus has at its disposal appropriate storage infrastructures and so we can store your goods in a safe and careful manner.\r\n</p>\r\n<p> <strong> The storage solutions are: </strong> </p>\r\n<ul>\r\n<li> Short term storage </li>\r\n<li> Long-term storage </li>\r\n<li> Car Storage </li>\r\n<li> Transit cargo storage </li>\r\n</ul>\r\n\r\n\r\n<hr/> \r\n\r\n<div class=\"row\">\r\n\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t<a href=\"public/img/gallery/armazenagem.jpg\" data-lightbox=\"gal-warehouse\">\r\n\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/armazenagem-thumb.jpg\" alt=\"\">\r\n\t\t</a>\r\n\t</div>\r\n\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t<a href=\"public/img/gallery/forklift.jpg\" data-lightbox=\"gal-warehouse\">\r\n\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/forklift-thumb.jpg\" alt=\"\">\r\n\t\t</a>\r\n\t</div>\r\n</div>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogOffice\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Office Moves</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Your company is thinking about changing office? </p>\r\n<p> In Invictus we offer solutions for quick and thoughtful change, so that your work does not stop. </p>\t\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogArtworks\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Artwork transportation</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nYour own a work of art whose value is priceless? Consider changing is worrisome? In Invictus you will find specialized personnel for the transport of all types\r\nof art works, ensuring that the process that seemed complicated is very straightforward.\r\n</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogEmpty\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Empty</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\tTODO\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogPets\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Pet Moving</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> We know that pets are like family members, so their transport can\r\nworry owners. However, with our service\r\nof animals transport your\r\ncompanion will be transported safely and will\r\ncome with optimum health. </p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogRelocation\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Relocation</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nWe know that moving to a new country, new culture, new language, is a task that can\r\nlook complicated and still brings concerns like how to find the ideal home,\r\ntransportation, school for the children, etc. </p>\r\n\r\n<p> Invictus Relocation offers guests a full-service Relocation in which they are\r\ninclude the following services: </p>\r\n<ul>\r\n<li> Treatment Visa / Immigration </li>\r\n<li> Local Guidance </li>\r\n<li> Temporary Accommodation </li>\r\n<li> School Search </li>\r\n<li> Residence Search </li>\r\n<li> Babysitting </li>\r\n<li> Furniture Rental </li>\r\n<li> Healthcare </li>\r\n<li> Car Register </li>\r\n<li> Taxi Service </li>\r\n</ul>\t\t\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogFacilities\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Our Facilities</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Our facilities are located in a private condominium with video surveillance system and image recording. </p>\r\n<p> The whole area is covered by fire, smoke and CO2 detectors connected to a central alarm station, police and fire departments within walking distance and fire-fighting systems. </p>\r\n<p> Large area for loading and unloading cargo as well as parking slots for our customers. </p>\r\n<p> Close to the main access roads. ( A5 / IC19 / A16 ) </p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogMaterials\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Our Materials</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t<div class=\"row\">\r\n\t<div class=\"col-sm-6 vcenter\">\r\n\t\t<ul style=\"margin-bottom:0px\">\r\n\t\t\t\r\n<li>Boxes with hangers for clothes</li>\r\n<li>Filling Foam</li>\r\n<li>Paper cover</li>\r\n<li>Wooden crates</li>\r\n<li>Frame boxes</li>\r\n<li>Boxes with dividers for glasses</li>\r\n<li>Wine boxes</li>\r\n<li>Bubble Wrap</li>\r\n<li>Wooden boxes for valuable crockery</li>\r\n<li>Boxes for seats</li>\r\n<li>Boxes for books</li>\r\n<li>High boxes appropriated for lamps</li>\r\n<li>Steel Strap</li>\r\n<li>“Magic Box”</li>\r\n<li>Boxes for lampshades</li>\r\n<li>Display tubes</li>\r\n<li>Wrapping paper for crockery and decoration items</li>\r\n<li>Blankets</li>\r\n\r\n\t\t</ul>\r\n\t</div><!--\r\n\t--><div class=\"col-sm-6 vcenter\">\r\n\t\t<img class=\"img-responsive img-portfolio img-rounded\" src=\"public/img/materials.jpg\" alt=\"\">\r\n\t</div>\r\n</div>\r\n\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t\n\t\n<script src=\"http://code.jquery.com/jquery-2.1.1.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>\n\t\n<!-- build:js public/js/combined.js -->\n<script src=\"public/js/lightbox.min.js\"></script>\n<script src=\"public/js/jquery.form.min.js\"></script>\n<script src=\"public/js/jquery.bootstrap-growl.min.js\"></script>\n<script src=\"public/js/app.js\"></script>\n<!-- endbuild -->\n\n\n</body>\n\n</html>";
	this.ptHomePage = "<!DOCTYPE html>\n\n<html lang=\"pt\">\n\n\n<head>\n\n\t<meta charset=\"utf-8\">\n\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\t<meta name=\"description\" content=\"\">\n\t<meta name=\"author\" content=\"\">\n\n\t<title>Invictus Relocation</title>\n\t\n\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">\n\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css\">\n\t<!-- build:css public/css/combined.css -->\n\t<link rel=\"stylesheet\" href=\"public/css/main.css\" >\n\t<link rel=\"stylesheet\" href=\"public/css/lightbox.css\"  />\n\t<!-- endbuild -->\n\t<link rel=\"stylesheet\" href=\"public/font-awesome/css/font-awesome.min.css\">\n\n\t<script>\n\t\t(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n\t\t(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n\t\tm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n\t\t})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n\t\t\n\t\tga('create', 'UA-55271438-3', 'auto');\n\t\tga('send', 'pageview');\n\t</script>\n\t\t\n</head>\n\n<body>\n\n\t<div id=\"fb-root\"></div>\n\t<script>(function(d, s, id) {\n\tvar js, fjs = d.getElementsByTagName(s)[0];\n\tif (d.getElementById(id)) return;\n\tjs = d.createElement(s); js.id = id;\n\tjs.src = \"//connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.0\";\n\tfjs.parentNode.insertBefore(js, fjs);\n\t}(document, 'script', 'facebook-jssdk'));</script>\n\n\t\n\t<nav class=\"navbar navbar-default\" role=\"navigation\">\n\t\t<div class=\"container\">\n\n\t\t\t<div class=\"navbar-header\">\n\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t</button>\n\t\t\t\t<a class=\"navbar-brand\" href=\"#\">Invictus Relocation</a>\n\t\t\t</div>\n\n\t\t\t<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\t\t\t\t<ul class=\"nav navbar-nav\">\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#about\">Sobre Nós</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#services\">Serviços</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#facilities\">Instalações</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#materials\">Materiais</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#contacts\">Contactos</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\n\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#budget\">Orçamento</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\t<a href=\"#tracking\">Tracking</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"dropdown\">\n\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><span class=\"label label-success\">\n\t\t\t\t\t\t\tPortuguês\n\t\t\t\t\t\t</span><span class=\"caret\"></span></a>\n\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">\n\t\t\t\t\t\t\t<li><a href=\"?lang=pt\" class=\"text-right\">Português (PT)</a></li>\n\t\t\t\t\t\t\t<li><a href=\"?lang=en\" class=\"text-right\">English (EN)</a></li>\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t</li>\n\t\t\t\t\t\n\t\t\t\t</ul>\n\n\t\t\t</div>\n\n\t\t</div>\n\t</nav>\n\n\t<header class=\"business-header\">\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t\t<h1 class=\"tagline\">Your Relocation</h1>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\n\t<!-- Heading Row -->\n\t<div class=\"container\" style=\"margin-top:40px\">\n\n\t\t<div class=\"row\" id=\"about\">\n\t\t\t<div class=\"col-lg-12\">\n\t\t\t\t<h1 class=\"text-center\" style=\"margin-bottom:40px\">Bem Vindo à Invictus Relocation</h1>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class=\"col-sm-6 col-sm-push-3 text-center\" style=\"margin-bottom:10px\">\n\t\t\t\t<img class=\"img-responsive img-rounded\" src=\"public/img/invictus-logo.png\" alt=\"\" style=\"margin-top:10px;margin-bottom:20px\">\n\t\t\t\t<p> A Invictus Relocation é a nova escolha nos serviços de mudança, fornecemos serviços com qualidade e ao mais alto nível do setor.\n\t\t\t\t\t<br>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<!--SERVIÇOS -->\n\t<div class=\"container\">\n\t\t<hr>\n\t</div>\n\n\t<div class=\"container\" style=\"margin-bottom:40px\" id=\"services\">\n\t<div class=\"row w-margin\">\n\t\t<div class=\"col-lg-12\">\n\t\t\t<h1 class=\"text-center\" style=\"margin-bottom:40px;margin-top:-20px\">\n\t\t\t\tOs Nossos Serviços\n\t\t\t</h1>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t\t<i class=\"fa fa-truck fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t\t<h4 class=\"service-title\">Mudanças<br>Internacionas</h4>\n\t\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Tornamos o processo de mudança internacional numa tarefa simples e cómoda.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogInternational\" data-title=\"Mudanças Internacionais\" data-content=\"testInfo\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-flag fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">Mudanças<br>Nacionais</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Oferecemos várias soluções para mudanças dentro de território nacional.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogNational\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-compass fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">Soluções de<br>Logística</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Na Invictus pomos à sua disposição toda a logística ligada ao transporte de todos os seus bens.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogLogistics\">Saber Mais</a>\n\t\t</div>\n\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t\t<i class=\"fa fa-plane fa-stack-1x fa-inverse\"></i>\n\t\t\t\t\t<h4 class=\"service-title\">Transportes Aéreos<br>Marítimos e Terrestres</h4>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Colocamos à sua escolha diferentes meios de transporte para a sua encomenda.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogTransportation\">Saber Mais</a>\n\t\t</div>\n\t</div>\n\n\t<!-- /.row -->\n\t<div class=\"row w-margin\" style=\"padding-top:20px\">\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-archive fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Armazenagem</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Fornecemos soluções de armazenagem com qualidade e segurança ao mais alto nível.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogStorage\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-fax fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Mudanças de<br>Escritório</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Transferimos o seu escritório para outro local de forma rápida e cuidadosa.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogOffice\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-paint-brush fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Transporte de<br>Obras de Arte</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Asseguramos o cuidado no transporte de obras de arte e de outros bens frágeis.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogArtworks\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-plus fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Serviços Extra</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Dispomos ainda uma vasta gama de serviços complementares.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogExtras\">Saber Mais</a>\n\t\t</div>\n\t</div>\n\t\n\t<!-- /.row -->\n\t<div class=\"row w-margin\" style=\"padding-top:20px\">\n\t\t<div class=\"col-sm-3\">\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-paw fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Transporte de<br>Animais de Estimação</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Tratamos do transporte dos seus animais de estimação com o maior cuidado.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogPets\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t\t<div class=\"service-header\">\n\t\t\t\t<span class=\"fa-stack fa-2x\">\n\t\t\t\t<i class=\"fa fa-circle fa-stack-2x\"></i>\n\t\t\t\t<i class=\"fa fa-home fa-stack-1x fa-inverse\"></i>\n\t\t\t\t<h4 class=\"service-title\">Relocation</h4>\n\t\t\t</span>\n\t\t\t</div>\n\t\t\t<p>Colocamos ao seu dispor um serviço completo de Relocation.</p>\n\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogRelocation\">Saber Mais</a>\n\t\t</div>\n\t\t<div class=\"col-sm-3\">\n\t\t</div>\n\t</div>\n</div>\n\t\n\t<!-- /.SERVICES -->\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\t\n\t<header class=\"background-img1\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"facilities\">\n\t\t\t<div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">As Nossas Instalações</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<p>As nossas instalações encontram-se bem situadas e possuem o equipamento e as condições necessárias para atender as diversas operações de logística.</p>\n\t\t\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogFacilities\">Saber Mais</a>\n\t\t\t\t</div>\n\t\t\t</div><!-- \n   \t\t\t\n\t\t--><div class=\"col-sm-2 vcenter\"></div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/office.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/office-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/warehouse2.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse2-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t\t<a href=\"public/img/gallery/warehouse.jpg\" data-lightbox=\"inst\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\t<header class=\"background-img2\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"materials\">\n\t\t\t<div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/materials.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/materials-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t\t<a href=\"public/img/gallery/truck.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/truck-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-3 vcenter\">\n\t\t\t\t<a href=\"public/img/gallery/warehouse3.jpg\" data-lightbox=\"mat\">\n\t\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/warehouse3-thumb.jpg\" alt=\"\">\n\t\t\t\t</a>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-2 vcenter\">\n\t\t   </div><!--\n\t\t--><div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">Os Nossos Materiais</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<p>Os materiais usados no transporte e armazenamento das encomendas devem ser apropriados, para isso Dispomos de a uma grande variedade de soluções.</p>\n\t\t\t\t\t<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#dialogMaterials\">Saber Mais</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\n\t<div class=\"container\">\n\t\t<hr class=\"wo-margin\">\n\t</div>\n\n\t<header class=\"background-img3\">\n\t<div class=\"container\">\n\t\t<div class=\"row w-margin\" id=\"contacts\">\n\t\t\t<div class=\"col-sm-4 vcenter\">\n\t\t\t\t<h1 class=\"feature-header\">Contactos</h1>\n\t\t\t\t<div class=\"feature-text\">\n\t\t\t\t\t<address>\n\t\t\t\t\t\t\tEdificio Invictus - Armazém C<br>\n\t\t\t\t\t\t\tAvenida Santa Isabel nº 4 e 8<br>\n\t\t\t\t\t\t\t2635-047 Cabra Figa<br>\n\t\t\t\t\t\t\tPortugal<br><br>\n\t\t\t\t\t\t\t<strong>Tel:</strong> (+351) 219 259 580<br>\n\t\t\t\t\t\t\t<strong>Fax:</strong> (+351) 219 259 582<br>\n\t\t\t\t\t\t\t<strong>Email:</strong> <a href=\"mailto:enquiries@invictus-relo.com\" target=\"_top\">enquiries@invictus-relo.com</a>\n\t\t\t\t\t\t</address>\n\t\t\t\t</div>\n\t\t\t</div><!--\n\t\t--><div class=\"col-sm-2 vcenter\"></div><!--\n\t\t--><div class=\"col-sm-6 vcenter google-maps\">\n\t\t\t\t<iframe frameborder=\"0\" style=\"border:0\" src=\"https://www.google.com/maps/embed/v1/streetview?location=38.746464,-9.342258&heading=90&pitch=3&key=AIzaSyB9eaq75nzfAMcM8uF-NGq1AikRakgO0oY\"></iframe>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</header>\n\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\n\t<!-- ORCAMENTO -->\n\t<header>\n\t\t<div class=\"\">\n\t\t\t<div class=\"budget-holder jumbotron\" id=\"budget\">\n\t\t\t\t<div class=\"row w-margin\">\n\t\t\t\t\t<div class=\"col-lg-12 text-center\">\n\t\t\t\t\t\t<h3>Preencha o nosso formulário e receba gratuitamente um orçamento personalizado.</h3>\n\t\t\t\t\t\t<span id=\"btnRequestBudget\" class=\"btn btn-lg btn-primary\" href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#budgetDialog\" style=\"margin-top:20px\">\n\t\t\t\t\t\tPedir Orçamento\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\t\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\t\n\t<!-- REDES SOCIAIS -->\n\t<header class=\"background-img2\">\n\t\t<div class=\"container\" style=\"padding-bottom:20px;padding-top:20px\" id=\"social\">\n\t\t\t<div class=\"col-xs-6 vcenter\">\n\t\t\t\t<div style=\"padding-right:20px; padding-top:20px; padding-bottom:20px\">\n\t\t\t\t<div class=\"fb-like-box\" data-href=\"https://www.facebook.com/InvictusRelocationServices\" data-colorscheme=\"light\" data-show-faces=\"false\" data-header=\"false\" data-width=\"auto\" data-stream=\"true\" data-show-border=\"true\"></div>\n\t\t\t\t</div>\n\t\t\t</div><!--\n\t\t\t--><div class=\"col-xs-6 vcenter\">\n\t\t\t\t<h2 class=\"feature-header\">Encontre-nos também nas redes-sociais</h2>\n\t\t\t\t<div class=\"text-center\">\n\t\t\t\t\t<!--p>[Facebook][Twitter]</p-->\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</header>\n\t\n\t<!-- ORDER TRACKING -->\n\t<div class=\"container\">\n\t\t<div class=\"row jumbotron tracking-container\" id=\"tracking\">\n\t\t\t<div class=\"container\">\n\t\t\t<div class=\"col-xs-6 vcenter\">\n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<h2 class=\"feature-header white-text\">Localize a sua mudança</h2>\n\t\t\t\t</div>\n\t\t\t\t<h4 class=\"white-text\" style=\"line-height:35px;\">\n\t\t\t\tSe já tem uma encomenda, insira aqui o seu <strong>código de localização</strong> para consultar o seu estado e localização.\t\n\t\t\t\t</h4>\n\t\t\t</div><!-- \n\t\t\t<--><div class=\"col-xs-6 vcenter text-center\">\n\t\t\t\t<form id=\"trackingForm\" class=\"form-inline\" method=\"post\" action=\"orderTracking\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t<div class=\"input-group-addon\">\n\t\t\t\t\t\t\t\t<span><i class=\"fa fa-info-circle fa-lg\"></i></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" id=\"code\" name=\"code\" placeholder=\"Código de Tracking\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-success btn-lg\">Procurar</button>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\t\n\t\t\t\t<div id=\"trackingTxtError\" style=\"color:#FF0000;display:none\">Error</div>\n\t\t\t\t<div id=\"trackingPanel\" class=\"row\" style=\"margin-top:20px;display:none\">\n\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t<h4><small class=\"white-text\">Estado</small></h4>\n\t\t\t\t\t\t<div id=\"txtStatus\" class=\"well well-sm\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t<h4><small class=\"white-text\">Local</small></h4>\n\t\t\t\t\t\t<div id=\"txtLocation\" class=\"well well-sm\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<!--div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div>\n\t\n\t\n\t\n\t<div class=\"container\">\n\t\t<hr class='wo-margin'>\n\t</div-->\n\t\n\t<!-- CERTIFICADOS -->\n\t<div class=\"container\" style=\"padding-top:20px\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-xs-12 vcenter text-center\">\n\t\t\t\t<h4>Certificados por:</h4>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-xs-4 col-xs-offset-4\">\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-dod.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-iam.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<img src=\"public/img/certificate-imc.jpg\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t\n\t<div class=\"container\">\n\t<div class=\"row\" id='gallery'>\n\t\t<div class=\"col-lg-12\">\n\t\t\t<h1 class=\"page-header text-center\">Galeria</h1>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/associations.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/associations-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/boxes.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/boxes-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/cargo.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/cargo-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/morecargo.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/morecargo-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/office.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/office-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t\t<div class=\"col-md-2 col-sm-3\">\n\t\t\t<a href=\"public/img/gallery/viatura2.jpg\" data-lightbox=\"gal\">\n\t\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/viatura2-thumb.jpg\" alt=\"\">\n\t\t\t</a>\n\t\t</div>\n\t</div>\n</div>\n\t\n\t<hr>\n\n\t<!-- Footer -->\n\t<footer>\n\t\t<div class=\"container\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-12 text-center\">\n\t\t\t\t\t<h4><small>Copyright &copy; InvictusRelo - TiagoLr 2015</small></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</footer>\n\n\t<!-- /.container -->\n\n\t<div id=\"budgetDialog\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t<div class=\"modal-dialog\">\n\t\t<div class=\"modal-content\">\n\t\t\t<div class=\"modal-header\">\n\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t</button>\n\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Pedido de Orçamento</h4>\n\t\t\t</div>\n\t\t\t<div class=\"modal-body\">\n\t\t\t\t<form id='budgetForm' class=\"form-horizontal\" method=\"post\" action=\"requestBudget\">\n\t\t\t\t\t<fieldset>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"name\" class=\"col-sm-3 control-label\">Nome*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"António José\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"email\" class=\"col-sm-3 control-label\">Email*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"exemplo@email.com\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"phone\" class=\"col-sm-3 control-label\">Telefone*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" placeholder=\"+351 808 910 100\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"origin\" class=\"col-sm-3 control-label\">Origem*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"origin\" name=\"origin\" placeholder=\"Lisboa, Portugal\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"destination\" class=\"col-sm-3 control-label\">Destino*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"destination\" name=\"destination\" placeholder=\"Paris, França\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"date\" class=\"col-sm-3 control-label\">Data da Mudança*</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<input type=\"date\" class=\"form-control\" id=\"date\" name=\"date\" required>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label for=\"comments\" class=\"col-sm-3 control-label\">Comentários</label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" id=\"comments\" name=\"comments\" placeholder=\"Coloque aqui comentários ou informações adicionais em relação ao seu pedido.\"></textarea>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<input type=\"hidden\" name=\"protection\" id=\"protection\">\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"dialog-error-msg\"></div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\"></label>\n\t\t\t\t\t\t\t<div class=\"col-sm-9\">\n\t\t\t\t\t\t\t\t<!--\t\t\t\t\t\t\t\t\t<div class=\"checkbox\">-->\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input id=\"human\" name=\"human\" type=\"checkbox\" value=\"true\"> Confirmo que os dados inseridos estão corretos.\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t<!--\t\t\t\t\t\t\t\t\t</div>-->\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<div class=\"col-sm-12 text-right\">\n\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>\n\t\t\t\t\t\t\t\t<button id=\"btnBFSubmit\" class=\"btn btn-primary\">Enviar Pedido</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t</fieldset>\n\t\t\t\t</form>\n\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n</div>\n\t\n\t<!-- INFO DIALOGS -->\n\t\n\t<div id=\"dialogInternational\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Mudanças Internacionais</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>Escolha o sítio para onde vai morar, a Invictus Relocation trata do resto.</p>\r\n<p>A escolha do método correto de transporte é uma parte vital do processo da mudança internacional. Como tal, a Invictus Relocation irá eleger (de acordo com as necessidades de cada cliente) o meio de transporte mais adequado. Pelo ar, terra ou mar a sua carga está segura. </p>\r\n<p><strong>Planeamos a sua mudança: </strong></p>\r\n<p>A Invictus designará um responsável comercial para fazer uma visita ao domicílio do cliente, para efetuar o levantamento das necessidades do cliente, verificar o volume a transportar e determinar o tipo de materiais de embalagem a utilizar para assegurar um serviço de qualidade. </p>\r\n<p><strong>Os nossos serviços incluem:</strong></p>\r\n<ul>\r\n<li>Inquérito junto do cliente</li>\r\n<li>Verificação do volume</li>\r\n<li>Verificação do acesso / condição para carga/descarga</li>\r\n<li>Proteção do chão e espaço da casa </li>\r\n<li>Embalagem de Exportação</li>\r\n<li>Realização de Inventário da Carga</li>\r\n<li>Etiquetagem de cada volume</li>\r\n</ul>\r\n<p><strong>Entrega no Destino:</strong></p>\r\n<p>\r\nA Invictus irá informá-lo sobre toda a documentação necessária para desalfandegar os seus bens no país de destino. Através de nossa rede mundial de agentes, estamos capacitados em entregar sua mudança em qualquer país do mundo.\r\n</p>\r\n\r\n\r\n<img class=\"img-responsive img-portfolio img-rounded\" src=\"public/img/worldmap.jpg\" alt=\"\">\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogNational\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Mudanças Nacionais</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>Escolha o sítio para onde vai morar, a Invictus Relocation trata do resto.</p>\r\n<p><strong>Planeamos a sua mudança: </strong></p>\r\n<p>A Invictus designará um responsável comercial para fazer uma visita ao domicílio do cliente, para efetuar o levantamento das necessidades do cliente, verificar o volume a transportar e determinar o tipo de materiais de embalagem a utilizar para assegurar um serviço de qualidade. </p>\r\n<p><strong>Os nossos serviços incluem:</strong></p>\r\n<ul>\r\n<li>Inquérito junto do cliente</li>\r\n<li>Verificação do volume</li>\r\n<li>Verificação do acesso / condição para carga/descarga</li>\r\n<li>Proteção do chão e espaço da casa </li>\r\n<li>Embalagem </li>\r\n<li>Realização de Inventário da Carga</li>\r\n<li>Etiquetagem de cada volume</li>\r\n</ul>\r\n\r\n<p><strong>Entrega no Destino:</strong></p>\r\n<p>Será efetuada a entrega, desembalagem, e montagem dos móveis (caso seja solicitado pelo cliente).</p>\r\n\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogExtras\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Serviços Extras</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>Como cada mudança é única, na Invictus oferecemos uma vasta gama de serviços extras de forma a satisfazer todas as suas necessidades pessoais: </p>\r\n\r\n<ul>\r\n<li>Transporte de pianos / afinação</li>\r\n<li>Remoção de bens indesejados </li>\r\n<li>Doações para organizações de caridade</li>\r\n<li>Transporte de veiculos</li>\r\n<li>Serviços de limpeza</li>\r\n<li>Serviço de carpinteiro, canalizador, eletricista</li>\r\n<li>Restauração de mobiliario</li>\r\n</ul>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogLogistics\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Soluções de logística</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>A Invictus oferece-lhe diversas opções de logistica:</p>\r\n\r\n<ul>\r\n<li>Via Rodoviária: Para Serviços intra europeus.</li>\r\n<li>Via Aérea: Para uma entrega mais rápida.</li>\r\n<li>Via Marítima: Para Serviços Internacionais </li>\r\n<li>LCL (Groupage Services) : Uso Parcial de Contentores para expedições menores.</li>\r\n<li>FCL (Full Container Load): Uso Exclusivo de Contentores.</li>\r\n</ul>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogTransportation\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Transportes</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nO método de transporte é uma parte vital do processo de mudança. A Invictus tratará com o cliente qual o método de transporte adequado. \r\nPelo ar, terra ou mar a sua carga está segura.\r\n</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogStorage\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Armazenagem</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>A Invictus tem ao seu dispor infraestruturas de armazenagem adequadas e \r\n\tpróprias para que possa guardar os seus bens de forma segura e cuidada.\r\n</p>\r\n<p><strong>As soluções de armazenagem são:</strong></p>\r\n<ul>\r\n<li>Armazenagem a curto prazo</li>\r\n<li>Armazenagem a longo prazo</li>\r\n<li>Armazenagem de viaturas</li>\r\n<li>Armazenagem de cargas em trânsito</li>\r\n</ul>\r\n\r\n\r\n<hr/> \r\n\r\n<div class=\"row\">\r\n\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t<a href=\"public/img/gallery/armazenagem.jpg\" data-lightbox=\"gal-warehouse\">\r\n\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/armazenagem-thumb.jpg\" alt=\"\">\r\n\t\t</a>\r\n\t</div>\r\n\t<div class=\"col-md-3 col-sm-6\">\r\n\t\t<a href=\"public/img/gallery/forklift.jpg\" data-lightbox=\"gal-warehouse\">\r\n\t\t\t<img class=\"img-responsive img-portfolio img-hover img-rounded\" src=\"public/img/gallery/forklift-thumb.jpg\" alt=\"\">\r\n\t\t</a>\r\n\t</div>\r\n</div>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogOffice\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Mudanças de Escritório</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>A sua empresa está a pensar em mudar de escritório?</p> \r\n<p>Na Invictus oferecemos soluções para uma mudança rápida e segura, de forma a que o seu trabalho não pare.</p>\t\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogArtworks\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Transporte de Obras de Arte</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nTem uma obra de arte cujo valor é inestimável? Pensar em mudar é preocupante? Na Invictus encontrará pessoal especializado no transporte de todo o tipo \r\nde obras de arte, assegurando que o processo que parecia complicado se torna bastante simples.\r\n</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogEmpty\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Empty</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\tTODO\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogPets\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Transporte de animais de Estimação</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p> Sabemos que os animais de estimação são como membros da família, pelo que o seu transporte pode preocupar os donos. \r\nContudo, com os nossos serviços de transporte de animais tem a garantia de que o seu companheiro será transportado em segurança e de que \r\nchegará com óptima saúde.</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogRelocation\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Relocation</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>\r\nSabemos que a mudança para um novo país, nova cultura, nova língua, é uma tarefa que pode \r\nparecer complicada e acarreta ainda outras preocupações como encontrar a residência ideal, \r\ntransporte, escola para os filhos, etc.</p>\r\n\r\n<p>A Invictus Relocation coloca ao seu dispor um serviço completo de Relocation no qual estão \r\nincluídos, por exemplo, os seguintes serviços:</p>\r\n\r\n<ul>\r\n<li>Tratamento de Vistos / Imigração</li>\r\n<li>Orientação Local</li>\r\n<li>Alojamento Temporário</li>\r\n<li>Procura de Escola</li>\r\n<li>Procura de Residência</li>\r\n<li>Serviço de Babysitter</li>\r\n<li>Aluguer de Mobiliário</li>\r\n<li>Cuidados Médicos</li>\r\n<li>Registo de Viaturas</li>\r\n<li>Serviço de Táxi</li>\r\n</ul>\t\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogFacilities\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">As Nossas Instalações</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t\r\n<p>As nossas instalações localizam-se num condomínio fechado, com sistema de vídeo vigilância, \r\ncom gravação de imagens.</p>\r\n\r\n<p>Toda a área encontra-se coberta por detetores de incêndio, fumo e CO2, com comunicação \r\ndireta à Central de alarmes, GNR e Bombeiros, bem como meios próprios de extinção de \r\nincêndio.</p>\r\n\r\n<p>Amplo logradouro para cargas e descargas, bem como parque de estacionamento para os nossos clientes.</p>\r\n\r\n<p>Próximo às principais vias de acesso – A5 / IC19 / A16.</p>\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t<div id=\"dialogMaterials\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n\t\t<div class=\"modal-dialog modal-lg\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<h4 class=\"modal-title\">Os Nossos Materiais</h4>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body\">\n\t\t\t\t\t<div class=\"row\">\r\n\t<div class=\"col-sm-6 vcenter\">\r\n\t\t<ul style=\"margin-bottom:0px\">\r\n\t\t\t\r\n<li>Caixas de pendurados para colocação de roupa</li>\r\n<li>Esferovite de enchimento</li>\r\n<li>Manta de papel</li>\r\n<li>Caixa de madeir</li>\r\n<li>Caixas de quadros</li>\r\n<li>Caixas com divisórias para copos</li>\r\n<li>Caixas de vinho</li>\r\n<li>Papel bolha</li>\r\n<li>Caixa de madeira para loiças valiosas</li>\r\n<li>Caixa para cadeiras</li>\r\n<li>Caixa para livros</li>\r\n<li>Caixa alta apropriada para candeeiros</li>\r\n<li>Cinta de aço</li>\r\n<li>“Magic Box”</li>\r\n<li>Caixa para abajur</li>\r\n<li>Tubos para tela</li>\r\n<li>Papel para loiça</li>\r\n<li>Mantas</li>\r\n\r\n\t\t</ul>\r\n\t</div><!--\r\n\t--><div class=\"col-sm-6 vcenter\">\r\n\t\t<img class=\"img-responsive img-portfolio img-rounded\" src=\"public/img/materials.jpg\" alt=\"\">\r\n\t</div>\r\n</div>\r\n\r\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-footer\">\n\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Fechar</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\n\t\n\t\n<script src=\"http://code.jquery.com/jquery-2.1.1.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>\n\t\n<!-- build:js public/js/combined.js -->\n<script src=\"public/js/lightbox.min.js\"></script>\n<script src=\"public/js/jquery.form.min.js\"></script>\n<script src=\"public/js/jquery.bootstrap-growl.min.js\"></script>\n<script src=\"public/js/app.js\"></script>\n<!-- endbuild -->\n\n\n</body>\n\n</html>";
	var _g = this;
	buddy_BuddySuite.call(this);
	this.describe("when visiting homepage",function() {
		_g.it("should send portuguese version by default",function(done,__status) {
			js.JQuery.get("/",function(data) {
				buddy_ShouldString.should(data,__status).be(_g.ptHomePage,{ fileName : "TestClient.hx", lineNumber : 22, className : "TestClient", methodName : "new"});
				done();
			}).error(function(d) {
				_g.failSync(__status,d,{ fileName : "SuiteBuilder.hx", lineNumber : 134, className : "TestClient", methodName : "new"});
			});
		});
		_g.it("should send english version if '?lang=en'",function(done1,__status1) {
			js.JQuery.get("/?lang=en",function(data1) {
				buddy_ShouldString.should(data1,__status1).be(_g.enHomePage,{ fileName : "TestClient.hx", lineNumber : 28, className : "TestClient", methodName : "new"});
				done1();
			}).error(function(d1) {
				_g.failSync(__status1,d1,{ fileName : "SuiteBuilder.hx", lineNumber : 134, className : "TestClient", methodName : "new"});
			});
		});
		_g.it("should send portuguese version for languages not defined",function(done2,__status2) {
			js.JQuery.get("/?lang=endl",function(data2) {
				buddy_ShouldString.should(data2,__status2).be(_g.ptHomePage,{ fileName : "TestClient.hx", lineNumber : 34, className : "TestClient", methodName : "new"});
				done2();
			}).error(function(d2) {
				_g.failSync(__status2,d2,{ fileName : "SuiteBuilder.hx", lineNumber : 134, className : "TestClient", methodName : "new"});
			});
		});
	});
	this.describe("when requesting budget",function() {
		_g.it("should return 200OK if data is filled correctly");
		_g.it("should return 500Error if data is not filled correctly");
		_g.it("should return 500Error if spam is detected");
		_g.it("should accept any string as date");
		_g.after(function(done3,__status3) {
			done3();
		});
	});
	this.describe("when tracking an order",function() {
		_g.it("should display error message if code does not exist");
		_g.it("should display order location and status if it does exist");
		_g.after(function(done4,__status4) {
			done4();
		});
	});
};
$hxClasses["TestClient"] = TestClient;
TestClient.__name__ = ["TestClient"];
TestClient.__interfaces__ = [buddy_Buddy];
TestClient.main = function() {
	var reporter = new buddy_reporting_ConsoleReporter();
	var suites = [];
	var _g = 0;
	var _g1 = haxe_rtti_Meta.getType(Type.resolveClass("TestClient")).autoIncluded;
	while(_g < _g1.length) {
		var a = _g1[_g];
		++_g;
		suites.push(Type.createInstance(Type.resolveClass(a),[]));
	}
	var testsRunning = true;
	var runner = new buddy_SuitesRunner(suites,reporter);
	runner.run();
};
TestClient.__super__ = buddy_BuddySuite;
TestClient.prototype = $extend(buddy_BuddySuite.prototype,{
	__class__: TestClient
});
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = ["Type"];
Type.getClassName = function(c) {
	var a = c.__name__;
	if(a == null) return null;
	return a.join(".");
};
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw new js__$Boot_HaxeError("Too many arguments");
	}
	return null;
};
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw new js__$Boot_HaxeError("No such constructor " + constr);
	if(Reflect.isFunction(f)) {
		if(params == null) throw new js__$Boot_HaxeError("Constructor " + constr + " need parameters");
		return Reflect.callMethod(e,f,params);
	}
	if(params != null && params.length != 0) throw new js__$Boot_HaxeError("Constructor " + constr + " does not need parameters");
	return f;
};
var admin_FormsScreen = function() { };
$hxClasses["admin.FormsScreen"] = admin_FormsScreen;
admin_FormsScreen.__name__ = ["admin","FormsScreen"];
admin_FormsScreen.init = function() {
	admin_FormsScreen.formsPage = js.JQuery("#formsPage");
	admin_FormsScreen.btnRemoveForms = js.JQuery("#removeForms");
	admin_FormsScreen.btnRemoveForms.click(admin_FormsScreen.onRemoveForms);
	admin_FormsScreen.createTable();
	admin_FormsScreen.nameHolder = js.JQuery("#nameHolder");
	admin_FormsScreen.phoneHolder = js.JQuery("#phoneHolder");
	admin_FormsScreen.mailHolder = js.JQuery("#mailHolder");
	admin_FormsScreen.commentsHolder = js.JQuery("#commentsHolder");
};
admin_FormsScreen.createTable = function() {
	js.JQuery("#formsTable").bootstrapTable({ pagination : true, toolbar : "#formsToolbar", sortName : "id", sortOrder : "desc", url : "/admin/forms", showColumns : true, showRefresh : true, maintainSelected : false, columns : [{ field : "state", checkbox : true},{ title : "ID", field : "id", width : 30, sortable : true, visible : true},{ title : "Criado", field : "created", width : 70, formatter : utils_Table.dateFormatter, sortable : true},{ title : "IP", field : "ip", width : 70, sortable : true, visible : false},{ title : "Nome", field : "name", width : 200, sortable : true},{ title : "Email", field : "email", width : 100, sortable : true},{ title : "Tel", field : "phone", width : 70, sortable : true},{ title : "Origem", field : "orig", width : 100, sortable : true},{ title : "Destino", field : "dest", width : 100, sortable : true},{ title : "Inicio", field : "date", width : 70, formatter : utils_Table.dateFormatter, sortable : true},{ title : "Comentarios", field : "comments", visible : false}]}).on("all.bs.table",function(e,row) {
		haxe_Log.trace("EVT",{ fileName : "FormsScreen.hx", lineNumber : 112, className : "admin.FormsScreen", methodName : "createTable"});
		haxe_Log.trace(e,{ fileName : "FormsScreen.hx", lineNumber : 113, className : "admin.FormsScreen", methodName : "createTable"});
		admin_FormsScreen.countChecked();
	}).on("load-error.bs.table",function(data,status) {
		haxe_Log.trace(data,{ fileName : "FormsScreen.hx", lineNumber : 116, className : "admin.FormsScreen", methodName : "createTable"});
		haxe_Log.trace(status,{ fileName : "FormsScreen.hx", lineNumber : 117, className : "admin.FormsScreen", methodName : "createTable"});
	}).on("click-row.bs.table",function(row1,element) {
		var name = element.name;
		var comments = element.comments;
		var phone = element.phone;
		var mail = element.email;
		admin_FormsScreen.mailHolder.html(mail);
		admin_FormsScreen.nameHolder.html(name);
		admin_FormsScreen.phoneHolder.html(phone);
		admin_FormsScreen.commentsHolder.html(comments);
	});
};
admin_FormsScreen.countChecked = function() {
	var checked = js.JQuery("#formsTable").bootstrapTable("getSelections");
	if(checked.length > 0) admin_FormsScreen.btnRemoveForms.removeClass("disabled"); else admin_FormsScreen.btnRemoveForms.addClass("disabled");
};
admin_FormsScreen.onRemoveForms = function() {
	var checked = js.JQuery("#formsTable").bootstrapTable("getSelections");
	var ids;
	var _g = [];
	var _g1 = 0;
	while(_g1 < checked.length) {
		var row = checked[_g1];
		++_g1;
		_g.push(row.id);
	}
	ids = _g;
	js.JQuery.get("admin/forms/delete/" + JSON.stringify(ids),function(data) {
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	}).done(function(data2) {
		js.JQuery("#formsTable").bootstrapTable("refresh");
	});
};
var admin_OrdersScreen = function() { };
$hxClasses["admin.OrdersScreen"] = admin_OrdersScreen;
admin_OrdersScreen.__name__ = ["admin","OrdersScreen"];
admin_OrdersScreen.init = function() {
	admin_OrdersScreen.ordersPage = js.JQuery("#ordersPage");
	admin_OrdersScreen.btnAddOrder = js.JQuery("#addOrder");
	admin_OrdersScreen.btnRemoveOrder = js.JQuery("#removeOrder");
	admin_OrdersScreen.btnAddOrder.click(admin_OrdersScreen.onAddOrder);
	admin_OrdersScreen.btnRemoveOrder.click(admin_OrdersScreen.onRemoveOrders);
	admin_OrdersScreen.createTable();
};
admin_OrdersScreen.createTable = function() {
	js.JQuery("#ordersTable").bootstrapTable({ pagination : true, toolbar : "#ordersToolbar", search : true, url : "/admin/orders", sortName : "id", sortOrder : "desc", idField : "id", showColumns : true, showRefresh : true, maintainSelected : false, columns : [{ field : "state", checkbox : true},{ title : "ID", field : "id", width : 30, sortable : true, visible : false},{ title : "Ref#", field : "ref", width : 30, sortable : true, formatter : utils_Table.intFormatter, editable : { type : "text", url : "admin/orders/modify", name : "ref", error : function(r,v) {
		utils_Utils.handleUfError(r,"");
	}, params : function(params) {
		params.type = "int";
		return params;
	}}},{ title : "Cliente", field : "client", width : 170, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "client", error : function(r1,v1) {
		utils_Utils.handleUfError(r1,"");
	}}},{ title : "Agente", field : "agent", width : 170, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "agent", error : function(r2,v2) {
		utils_Utils.handleUfError(r2,"");
	}}},{ title : "Origem", field : "orig", width : 150, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "orig", error : function(r3,v3) {
		utils_Utils.handleUfError(r3,"");
	}}},{ title : "Destino", field : "dest", width : 150, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "dest", error : function(r4,v4) {
		utils_Utils.handleUfError(r4,"");
	}}},{ title : "Volume", field : "volume", width : 80, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "volume", error : function(r5,v5) {
		utils_Utils.handleUfError(r5,"");
	}}},{ title : "Preco", field : "budget", width : 80, sortable : true, formatter : utils_Table.currencyFormatter, editable : { type : "text", url : "admin/orders/modify", name : "budget", error : function(r6,v6) {
		utils_Utils.handleUfError(r6,"");
	}}},{ 'class' : "user-column", title : "Local", field : "location", width : 120, sortable : true, editable : { type : "text", url : "admin/orders/modify", name : "location", error : function(r7,v7) {
		utils_Utils.handleUfError(r7,"");
	}}},{ 'class' : "user-column", title : "Estado", field : "status", width : 100, sortable : true, editable : { type : "select", source : [{ value : OrderStatus.Open, text : Std.string(OrderStatus.Open)},{ value : OrderStatus.Started, text : Std.string(OrderStatus.Started)},{ value : OrderStatus.Finished, text : Std.string(OrderStatus.Finished)},{ value : OrderStatus.Canceled, text : Std.string(OrderStatus.Canceled)}], url : "admin/orders/modify", name : "status", error : function(r8,v8) {
		utils_Utils.handleUfError(r8,"");
	}}},{ 'class' : "user-column", title : "Codigo", field : "code", width : 150, visible : true}]}).on("load-error.bs.table",function(data,status) {
		haxe_Log.trace(data,{ fileName : "OrdersScreen.hx", lineNumber : 174, className : "admin.OrdersScreen", methodName : "createTable"});
		haxe_Log.trace(status,{ fileName : "OrdersScreen.hx", lineNumber : 175, className : "admin.OrdersScreen", methodName : "createTable"});
	}).on("all.bs.table",function(e,row) {
		admin_OrdersScreen.countOrdersChecked();
	});
};
admin_OrdersScreen.countOrdersChecked = function() {
	var checked = js.JQuery("#ordersTable").bootstrapTable("getSelections");
	if(checked.length > 0) admin_OrdersScreen.btnRemoveOrder.removeClass("disabled"); else admin_OrdersScreen.btnRemoveOrder.addClass("disabled");
};
admin_OrdersScreen.onRemoveOrders = function() {
	var checked = js.JQuery("#ordersTable").bootstrapTable("getSelections");
	var ids;
	var _g = [];
	var _g1 = 0;
	while(_g1 < checked.length) {
		var row = checked[_g1];
		++_g1;
		_g.push(row.id);
	}
	ids = _g;
	js.JQuery.get("admin/orders/delete/" + JSON.stringify(ids),function(data) {
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	}).done(function(data2) {
		js.JQuery("#ordersTable").bootstrapTable("refresh");
	});
};
admin_OrdersScreen.onAddOrder = function() {
	var args = { client : null, agent : null, orig : null, dest : null, volume : null, location : null, budget : 0};
	js.JQuery.post("admin/orders/create",args,function(data) {
		js.JQuery("#ordersTable").bootstrapTable("refresh");
	}).fail(function(data1,status) {
		utils_Utils.handleUfError(data1,status);
	});
};
var buddy_TestStatus = { __ename__ : true, __constructs__ : ["Unknown","Passed","Pending","Failed"] };
buddy_TestStatus.Unknown = ["Unknown",0];
buddy_TestStatus.Unknown.toString = $estr;
buddy_TestStatus.Unknown.__enum__ = buddy_TestStatus;
buddy_TestStatus.Passed = ["Passed",1];
buddy_TestStatus.Passed.toString = $estr;
buddy_TestStatus.Passed.__enum__ = buddy_TestStatus;
buddy_TestStatus.Pending = ["Pending",2];
buddy_TestStatus.Pending.toString = $estr;
buddy_TestStatus.Pending.__enum__ = buddy_TestStatus;
buddy_TestStatus.Failed = ["Failed",3];
buddy_TestStatus.Failed.toString = $estr;
buddy_TestStatus.Failed.__enum__ = buddy_TestStatus;
var buddy_TestStep = { __ename__ : true, __constructs__ : ["TSuite","TSpec"] };
buddy_TestStep.TSuite = function(s) { var $x = ["TSuite",0,s]; $x.__enum__ = buddy_TestStep; $x.toString = $estr; return $x; };
buddy_TestStep.TSpec = function(s) { var $x = ["TSpec",1,s]; $x.__enum__ = buddy_TestStep; $x.toString = $estr; return $x; };
var buddy_BeforeAfter = function(run,async) {
	if(async == null) async = false;
	this.run = run;
	this.async = async;
};
$hxClasses["buddy.BeforeAfter"] = buddy_BeforeAfter;
buddy_BeforeAfter.__name__ = ["buddy","BeforeAfter"];
buddy_BeforeAfter.prototype = {
	__class__: buddy_BeforeAfter
};
var buddy_Suite = function(name,buddySuite) {
	if(name == null) throw new js__$Boot_HaxeError("Suite requires a name.");
	if(buddySuite == null) throw new js__$Boot_HaxeError("Suite requires a BuddySuite.");
	this.name = name;
	this.buddySuite = buddySuite;
	this.before = new List();
	this.after = new List();
	this.steps = new List();
};
$hxClasses["buddy.Suite"] = buddy_Suite;
buddy_Suite.__name__ = ["buddy","Suite"];
buddy_Suite.prototype = {
	get_specs: function() {
		var output = new List();
		var _g_head = this.steps.h;
		var _g_val = null;
		while(_g_head != null) {
			var step;
			step = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			switch(step[1]) {
			case 1:
				var s = step[2];
				output.add(s);
				break;
			default:
			}
		}
		return output;
	}
	,get_suites: function() {
		var output = new List();
		var _g_head = this.steps.h;
		var _g_val = null;
		while(_g_head != null) {
			var step;
			step = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			switch(step[1]) {
			case 0:
				var s = step[2];
				output.add(s);
				break;
			default:
			}
		}
		return output;
	}
	,__class__: buddy_Suite
};
var buddy_Spec = function(suite,description,run,async,pending) {
	if(pending == null) pending = false;
	if(async == null) async = false;
	this.suite = suite;
	this.description = description;
	this.run = run;
	this.async = async;
	this.traces = new List();
	if(run == null) this.status = buddy_TestStatus.Pending; else if(pending) this.status = buddy_TestStatus.Pending; else this.status = buddy_TestStatus.Unknown;
};
$hxClasses["buddy.Spec"] = buddy_Spec;
buddy_Spec.__name__ = ["buddy","Spec"];
buddy_Spec.prototype = {
	setStatus: function(s,err,stack) {
		this.status = s;
		this.error = err;
		this.stack = stack;
	}
	,__class__: buddy_Spec
};
var buddy_Should = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	this.value = value;
	this.assert = assert;
	this.inverse = inverse;
};
$hxClasses["buddy.Should"] = buddy_Should;
buddy_Should.__name__ = ["buddy","Should"];
buddy_Should.prototype = {
	be: function(expected,p) {
		this.test(this.value == expected,p,"Expected " + this.quote(expected) + ", was " + this.quote(this.value),"Didn't expect " + this.quote(expected) + " but was equal to that");
	}
	,beType: function(type,p) {
		this.test(js_Boot.__instanceof(this.value,type),p,"Expected " + this.quote(this.value) + " to be type " + this.quote(type),"Expected " + this.quote(this.value) + " not to be type " + this.quote(type));
	}
	,quote: function(v) {
		if(typeof(v) == "string") return "\"" + Std.string(v) + "\""; else return Std.string(v);
	}
	,stackPos: function(p) {
		return [haxe_StackItem.FilePos(null,p.fileName,p.lineNumber)];
	}
	,test: function(expr,p,error,errorInverted) {
		if(!this.inverse) this.assert(expr,error,this.stackPos(p)); else this.assert(!expr,errorInverted,this.stackPos(p));
	}
	,__class__: buddy_Should
};
var buddy_ShouldDynamic = function(value,assert,inverse) {
	buddy_Should.call(this,value,assert,inverse);
};
$hxClasses["buddy.ShouldDynamic"] = buddy_ShouldDynamic;
buddy_ShouldDynamic.__name__ = ["buddy","ShouldDynamic"];
buddy_ShouldDynamic.should = function(d,assert) {
	return new buddy_ShouldDynamic(d,assert);
};
buddy_ShouldDynamic.__super__ = buddy_Should;
buddy_ShouldDynamic.prototype = $extend(buddy_Should.prototype,{
	get_not: function() {
		return new buddy_ShouldDynamic(this.value,this.assert,!this.inverse);
	}
	,__class__: buddy_ShouldDynamic
});
var buddy_ShouldInt = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	buddy_Should.call(this,value,assert,inverse);
};
$hxClasses["buddy.ShouldInt"] = buddy_ShouldInt;
buddy_ShouldInt.__name__ = ["buddy","ShouldInt"];
buddy_ShouldInt.should = function(i,assert) {
	return new buddy_ShouldInt(i,assert);
};
buddy_ShouldInt.__super__ = buddy_Should;
buddy_ShouldInt.prototype = $extend(buddy_Should.prototype,{
	get_not: function() {
		return new buddy_ShouldInt(this.value,this.assert,!this.inverse);
	}
	,beLessThan: function(expected,p) {
		this.test(this.value < expected,p,"Expected less than " + this.quote(expected) + ", was " + this.quote(this.value),"Expected not less than " + this.quote(expected) + ", was " + this.quote(this.value));
	}
	,beGreaterThan: function(expected,p) {
		this.test(this.value > expected,p,"Expected greater than " + this.quote(expected) + ", was " + this.quote(this.value),"Expected not greater than " + this.quote(expected) + ", was " + this.quote(this.value));
	}
	,__class__: buddy_ShouldInt
});
var buddy_ShouldFloat = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	buddy_Should.call(this,value,assert,inverse);
};
$hxClasses["buddy.ShouldFloat"] = buddy_ShouldFloat;
buddy_ShouldFloat.__name__ = ["buddy","ShouldFloat"];
buddy_ShouldFloat.should = function(i,assert) {
	return new buddy_ShouldFloat(i,assert);
};
buddy_ShouldFloat.__super__ = buddy_Should;
buddy_ShouldFloat.prototype = $extend(buddy_Should.prototype,{
	get_not: function() {
		return new buddy_ShouldFloat(this.value,this.assert,!this.inverse);
	}
	,beLessThan: function(expected,p) {
		this.test(this.value < expected,p,"Expected less than " + this.quote(expected) + ", was " + this.quote(this.value),"Expected not less than " + this.quote(expected) + ", was " + this.quote(this.value));
	}
	,beGreaterThan: function(expected,p) {
		this.test(this.value > expected,p,"Expected greater than " + this.quote(expected) + ", was " + this.quote(this.value),"Expected not greater than " + this.quote(expected) + ", was " + this.quote(this.value));
	}
	,beCloseTo: function(expected,precision,p) {
		if(precision == null) precision = 2;
		var expr = Math.abs(expected - this.value) < Math.pow(10,-precision) / 2;
		this.test(expr,p,"Expected close to " + this.quote(expected) + ", was " + this.quote(this.value),"Expected " + this.quote(this.value) + " not to be close to " + this.quote(expected));
	}
	,__class__: buddy_ShouldFloat
});
var buddy_ShouldString = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	buddy_Should.call(this,value,assert,inverse);
};
$hxClasses["buddy.ShouldString"] = buddy_ShouldString;
buddy_ShouldString.__name__ = ["buddy","ShouldString"];
buddy_ShouldString.should = function(str,assert) {
	return new buddy_ShouldString(str,assert);
};
buddy_ShouldString.__super__ = buddy_Should;
buddy_ShouldString.prototype = $extend(buddy_Should.prototype,{
	get_not: function() {
		return new buddy_ShouldString(this.value,this.assert,!this.inverse);
	}
	,contain: function(substring,p) {
		this.test(this.value.indexOf(substring) >= 0,p,"Expected " + this.quote(this.value) + " to contain " + this.quote(substring),"Expected " + this.quote(this.value) + " not to contain " + this.quote(substring));
	}
	,startWith: function(substring,p) {
		this.test(StringTools.startsWith(this.value,substring),p,"Expected " + this.quote(this.value) + " to start with " + this.quote(substring),"Expected " + this.quote(this.value) + " not to start with " + this.quote(substring));
	}
	,endWith: function(substring,p) {
		this.test(StringTools.endsWith(this.value,substring),p,"Expected " + this.quote(this.value) + " to end with " + this.quote(substring),"Expected " + this.quote(this.value) + " not to end with " + this.quote(substring));
	}
	,match: function(regexp,p) {
		this.test(regexp.match(this.value),p,"Expected " + this.quote(this.value) + " to match regular expression","Expected " + this.quote(this.value) + " not to match regular expression");
	}
	,__class__: buddy_ShouldString
});
var buddy_ShouldIterable = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	buddy_Should.call(this,value,assert,inverse);
};
$hxClasses["buddy.ShouldIterable"] = buddy_ShouldIterable;
buddy_ShouldIterable.__name__ = ["buddy","ShouldIterable"];
buddy_ShouldIterable.should = function(value,assert) {
	return new buddy_ShouldIterable(value,assert);
};
buddy_ShouldIterable.__super__ = buddy_Should;
buddy_ShouldIterable.prototype = $extend(buddy_Should.prototype,{
	get_not: function() {
		return new buddy_ShouldIterable(this.value,this.assert,!this.inverse);
	}
	,contain: function(o,p) {
		this.test(Lambda.exists(this.value,function(el) {
			return el == o;
		}),p,"Expected " + this.quote(this.value) + " to contain " + this.quote(o),"Expected " + this.quote(this.value) + " not to contain " + this.quote(o));
	}
	,containAll: function(values,p) {
		var expr = true;
		var $it0 = $iterator(values)();
		while( $it0.hasNext() ) {
			var a = $it0.next();
			var a1 = [a];
			if(!Lambda.exists(this.value,(function(a1) {
				return function(v) {
					return v == a1[0];
				};
			})(a1))) {
				expr = false;
				break;
			}
		}
		this.test(expr,p,"Expected " + this.quote(this.value) + " to contain all of " + this.quote(values),"Expected " + this.quote(this.value) + " not to contain all of " + this.quote(values));
	}
	,containExactly: function(values,p) {
		var a = $iterator(this.value)();
		var b = $iterator(values)();
		var expr = true;
		while(a.hasNext() || b.hasNext()) if(a.next() != b.next()) {
			expr = false;
			break;
		}
		this.test(expr,p,"Expected " + this.quote(this.value) + " to contain exactly " + this.quote(values),"Expected " + this.quote(this.value) + " not to contain exactly " + this.quote(values));
	}
	,__class__: buddy_ShouldIterable
});
var buddy_ShouldFunctions = function(value,assert,inverse) {
	if(inverse == null) inverse = false;
	this.value = value;
	this.assert = assert;
	this.inverse = inverse;
};
$hxClasses["buddy.ShouldFunctions"] = buddy_ShouldFunctions;
buddy_ShouldFunctions.__name__ = ["buddy","ShouldFunctions"];
buddy_ShouldFunctions.should = function(value,assert) {
	return new buddy_ShouldFunctions(value,assert);
};
buddy_ShouldFunctions.prototype = {
	get_not: function() {
		return new buddy_ShouldFunctions(this.value,this.assert,!this.inverse);
	}
	,throwValue: function(v,p) {
		var expr = false;
		try {
			this.value();
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			expr = e == v;
		}
		this.test(expr,p,"Expected " + this.quote(this.value) + " to throw " + this.quote(v),"Expected " + this.quote(this.value) + " not to throw " + this.quote(v));
	}
	,throwType: function(type,p) {
		var expr = false;
		var name = null;
		try {
			this.value();
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			name = Type.getClassName(type);
			expr = js_Boot.__instanceof(e,type);
		}
		this.test(expr,p,"Expected " + this.quote(this.value) + " to throw type " + name,"Expected " + this.quote(this.value) + " not to throw type " + name);
	}
	,be: function(expected,p) {
		this.test(this.value == expected,p,"Expected " + this.quote(expected) + ", was " + this.quote(this.value),"Didn't expect " + this.quote(expected) + " but was equal to that");
	}
	,quote: function(v) {
		if(typeof(v) == "string") return "\"" + Std.string(v) + "\""; else return Std.string(v);
	}
	,stackPos: function(p) {
		return [haxe_StackItem.FilePos(null,p.fileName,p.lineNumber)];
	}
	,test: function(expr,p,error,errorInverted) {
		if(!this.inverse) this.assert(expr,error,this.stackPos(p)); else this.assert(!expr,errorInverted,this.stackPos(p));
	}
	,__class__: buddy_ShouldFunctions
};
var buddy_SuitesRunner = function(buddySuites,reporter) {
	var includeMode;
	includeMode = ((function($this) {
		var $r;
		var _g = [];
		var $it0 = $iterator(buddySuites)();
		while( $it0.hasNext() ) {
			var b = $it0.next();
			var _g1_head = b.suites.h;
			var _g1_val = null;
			while(_g1_head != null) {
				var s;
				s = (function($this) {
					var $r;
					_g1_val = _g1_head[0];
					_g1_head = _g1_head[1];
					$r = _g1_val;
					return $r;
				}($this));
				if(s.include) _g.push(s);
			}
		}
		$r = _g;
		return $r;
	}(this))).length > 0;
	var _g1 = [];
	var $it1 = $iterator(buddySuites)();
	while( $it1.hasNext() ) {
		var b1 = $it1.next();
		var _g2_head = b1.suites.h;
		var _g2_val = null;
		while(_g2_head != null) {
			var s1;
			s1 = (function($this) {
				var $r;
				_g2_val = _g2_head[0];
				_g2_head = _g2_head[1];
				$r = _g2_val;
				return $r;
			}(this));
			if(!includeMode || s1.include) _g1.push(s1);
		}
	}
	this.suites = _g1;
	if(reporter == null) this.reporter = new buddy_reporting_ConsoleReporter(); else this.reporter = reporter;
};
$hxClasses["buddy.SuitesRunner"] = buddy_SuitesRunner;
buddy_SuitesRunner.__name__ = ["buddy","SuitesRunner"];
buddy_SuitesRunner.prototype = {
	run: function() {
		var _g = this;
		var def = new promhx_Deferred();
		var defPr = def.promise();
		this.reporter.start().then(function(ok) {
			if(ok) buddy_tools_AsyncTools.iterateAsyncBool(_g.suites,$bind(_g,_g.runSuite)).pipe(function(_) {
				return _g.reporter.done(_g.suites,!_g.failed());
			}).then(function(_1) {
				def.resolve(ok);
			}); else {
				_g.aborted = true;
				def.resolve(ok);
			}
		});
		return defPr;
	}
	,failed: function() {
		var testFail = null;
		testFail = function(s) {
			var failed = false;
			var _g_head = s.steps.h;
			var _g_val = null;
			while(_g_head != null) {
				var step;
				step = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				switch(step[1]) {
				case 1:
					var sp = step[2];
					if(sp.status == buddy_TestStatus.Failed) return true;
					break;
				case 0:
					var s2 = step[2];
					if(testFail(s2)) return true;
					break;
				}
			}
			return false;
		};
		var $it0 = $iterator(this.suites)();
		while( $it0.hasNext() ) {
			var s1 = $it0.next();
			if(testFail(s1)) return true;
		}
		return false;
	}
	,statusCode: function() {
		if(this.aborted) return 1;
		if(this.failed()) return 1; else return 0;
	}
	,runSuite: function(suite) {
		return new buddy_internal_SuiteRunner(suite,this.reporter).run();
	}
	,__class__: buddy_SuitesRunner
};
var buddy_internal_SuiteRunner = function(suite,reporter) {
	this.buddySuite = suite.buddySuite;
	this.suite = suite;
	this.reporter = reporter;
};
$hxClasses["buddy.internal.SuiteRunner"] = buddy_internal_SuiteRunner;
buddy_internal_SuiteRunner.__name__ = ["buddy","internal","SuiteRunner"];
buddy_internal_SuiteRunner.prototype = {
	run: function() {
		var _g = this;
		var traceFunc = haxe_Log.trace;
		var def = new promhx_Deferred();
		var pr = def.promise();
		buddy_tools_AsyncTools.iterateAsyncBool(this.suite.steps,$bind(this,this.runSteps)).then(function(_) {
			haxe_Log.trace = traceFunc;
			def.resolve(_g.suite);
		});
		return pr;
	}
	,allBefores: function(suite,list) {
		list = Lambda.concat(suite.before,list);
		if(suite.parent != null) return this.allBefores(suite.parent,list); else return Lambda.concat(this.buddySuite.befores,list);
	}
	,allAfters: function(suite,list) {
		list = Lambda.concat(suite.after,list);
		if(suite.parent != null) return this.allAfters(suite.parent,list);
		list = Lambda.concat(this.buddySuite.afters,list);
		var output = new List();
		var _g_head = list.h;
		var _g_val = null;
		while(_g_head != null) {
			var a;
			a = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			output.push(a);
		}
		return output;
	}
	,runBeforeAfter: function(b) {
		var def = new promhx_Deferred();
		var pr = def.promise();
		var done = function(calledFromSpec) {
			if(calledFromSpec == null) calledFromSpec = false;
			def.resolve(b);
		};
		b.run(done,function(s,err,stack) {
		});
		if(!b.async) done();
		return pr;
	}
	,runSteps: function(step) {
		var stepDone = new promhx_Deferred();
		var stepPr = stepDone.promise();
		switch(step[1]) {
		case 1:
			var spec = step[2];
			this.runSpec(spec).then(function(_) {
				stepDone.resolve(step);
			});
			break;
		case 0:
			var s = step[2];
			new buddy_internal_SuiteRunner(s,this.reporter).run().then(function(_1) {
				stepDone.resolve(step);
			});
			break;
		}
		return stepPr;
	}
	,runSpec: function(spec) {
		var _g = this;
		var specDone = new promhx_Deferred();
		var specPr = specDone.promise();
		specPr.pipe(function(s1) {
			if(_g.reporter != null) return _g.reporter.progress(s1); else return specPr;
		});
		if(spec.status != buddy_TestStatus.Unknown) {
			specDone.resolve(spec);
			return specPr;
		}
		var itDone = new promhx_Deferred();
		var itPromise = itDone.promise();
		var hasStatus = false;
		var status = function(s,error,stack) {
			hasStatus = true;
			if(!s && !itPromise._resolved && !itDone._resolved) itDone.resolve({ status : buddy_TestStatus.Failed, error : error, stack : stack});
		};
		var done = function(calledFromSpec) {
			if(calledFromSpec == null) calledFromSpec = true;
			if(!itPromise._resolved && !itDone._resolved) {
				if(calledFromSpec) hasStatus = true;
				itDone.resolve({ status : hasStatus?buddy_TestStatus.Passed:buddy_TestStatus.Pending, error : null, stack : null});
			}
		};
		haxe_Log.trace = function(v,pos) {
			spec.traces.add(pos.fileName + ":" + pos.lineNumber + ": " + Std.string(v));
		};
		var befores = this.allBefores(this.suite,new List());
		var afters = this.allAfters(this.suite,new List());
		var errorTimeout = null;
		buddy_tools_AsyncTools.iterateAsyncBool(befores,$bind(this,this.runBeforeAfter)).pipe(function(_) {
			if(spec.async) {
				var timeout = _g.buddySuite.timeoutMs;
				errorTimeout = buddy_tools_AsyncTools.wait(timeout);
				errorTimeout.catchError(function(e) {
					if(e != null) throw new js__$Boot_HaxeError(e);
				}).then(function(_1) {
					status(false,"Timeout after " + timeout + " ms",null);
				});
			}
			try {
				spec.run(done,status);
				if(!spec.async) done(false);
			} catch( e1 ) {
				haxe_CallStack.lastException = e1;
				if (e1 instanceof js__$Boot_HaxeError) e1 = e1.val;
				status(false,Std.string(e1),haxe_CallStack.exceptionStack());
			}
			return itPromise;
		}).pipe(function(result) {
			if(errorTimeout != null) {
				errorTimeout.reject(null);
				errorTimeout = null;
			}
			spec.setStatus(result.status,result.error,result.stack);
			return buddy_tools_AsyncTools.iterateAsyncBool(afters,$bind(_g,_g.runBeforeAfter));
		}).then(function(_2) {
			specDone.resolve(spec);
		});
		return specPr;
	}
	,__class__: buddy_internal_SuiteRunner
};
var buddy_internal_sys_Js = function() { };
$hxClasses["buddy.internal.sys.Js"] = buddy_internal_sys_Js;
buddy_internal_sys_Js.__name__ = ["buddy","internal","sys","Js"];
buddy_internal_sys_Js.replaceSpace = function(s) {
	if(window.navigator.userAgent.indexOf("PhantomJS") >= 0) return s;
	return StringTools.replace(s," ","&nbsp;");
};
buddy_internal_sys_Js.print = function(s) {
	var sp;
	var _this = window.document;
	sp = _this.createElement("span");
	sp.innerHTML = buddy_internal_sys_Js.replaceSpace(s);
	window.document.body.appendChild(sp);
};
buddy_internal_sys_Js.println = function(s) {
	var div;
	var _this = window.document;
	div = _this.createElement("div");
	div.innerHTML = buddy_internal_sys_Js.replaceSpace(s);
	window.document.body.appendChild(div);
};
var buddy_reporting_Reporter = function() { };
$hxClasses["buddy.reporting.Reporter"] = buddy_reporting_Reporter;
buddy_reporting_Reporter.__name__ = ["buddy","reporting","Reporter"];
buddy_reporting_Reporter.prototype = {
	__class__: buddy_reporting_Reporter
};
var buddy_reporting_TraceReporter = function() {
};
$hxClasses["buddy.reporting.TraceReporter"] = buddy_reporting_TraceReporter;
buddy_reporting_TraceReporter.__name__ = ["buddy","reporting","TraceReporter"];
buddy_reporting_TraceReporter.__interfaces__ = [buddy_reporting_Reporter];
buddy_reporting_TraceReporter.prototype = {
	start: function() {
		return this.resolveImmediately(true);
	}
	,progress: function(spec) {
		return this.resolveImmediately(spec);
	}
	,done: function(suites,status) {
		var _g = this;
		this.println("");
		var total = 0;
		var failures = 0;
		var pending = 0;
		var countTests = null;
		var printTests = null;
		countTests = function(s) {
			var _g_head = s.steps.h;
			var _g_val = null;
			while(_g_head != null) {
				var sp;
				sp = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				switch(sp[1]) {
				case 1:
					var sp1 = sp[2];
					total++;
					if(sp1.status == buddy_TestStatus.Failed) failures++; else if(sp1.status == buddy_TestStatus.Pending) pending++;
					break;
				case 0:
					var s1 = sp[2];
					countTests(s1);
					break;
				}
			}
		};
		Lambda.iter(suites,countTests);
		printTests = function(s2,indentLevel) {
			var print = function(str) {
				_g.println(StringTools.lpad(str," ",str.length + indentLevel * 2));
			};
			print(s2.name);
			var _g_head1 = s2.steps.h;
			var _g_val1 = null;
			while(_g_head1 != null) {
				var step;
				step = (function($this) {
					var $r;
					_g_val1 = _g_head1[0];
					_g_head1 = _g_head1[1];
					$r = _g_val1;
					return $r;
				}(this));
				switch(step[1]) {
				case 1:
					var sp2 = step[2];
					if(sp2.status == buddy_TestStatus.Failed) {
						print("  " + sp2.description + " (FAILED: " + sp2.error + ")");
						_g.printTraces(sp2);
						if(sp2.stack == null || sp2.stack.length == 0) continue;
						var _g1 = 0;
						var _g2 = sp2.stack;
						while(_g1 < _g2.length) {
							var s3 = _g2[_g1];
							++_g1;
							switch(s3[1]) {
							case 2:
								var line = s3[4];
								var file = s3[3];
								if(file.indexOf("buddy/internal/") != 0) print("    @ " + file + ":" + line); else {
								}
								break;
							default:
							}
						}
					} else {
						print("  " + sp2.description + " (" + Std.string(sp2.status) + ")");
						_g.printTraces(sp2);
					}
					break;
				case 0:
					var s4 = step[2];
					printTests(s4,indentLevel + 1);
					break;
				}
			}
		};
		Lambda.iter(suites,(function(f,a2) {
			return function(a1) {
				f(a1,a2);
			};
		})(printTests,0));
		this.println("" + total + " specs, " + failures + " failures, " + pending + " pending");
		return this.resolveImmediately(suites);
	}
	,printTraces: function(spec) {
		var _g_head = spec.traces.h;
		var _g_val = null;
		while(_g_head != null) {
			var t;
			t = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			this.println("    " + t);
		}
	}
	,print: function(s) {
	}
	,println: function(s) {
		haxe_Log.trace(s,{ fileName : "TraceReporter.hx", lineNumber : 105, className : "buddy.reporting.TraceReporter", methodName : "println"});
	}
	,resolveImmediately: function(o) {
		var def = new promhx_Deferred();
		var pr = def.promise();
		def.resolve(o);
		return pr;
	}
	,__class__: buddy_reporting_TraceReporter
};
var buddy_reporting_ConsoleReporter = function() {
	buddy_reporting_TraceReporter.call(this);
};
$hxClasses["buddy.reporting.ConsoleReporter"] = buddy_reporting_ConsoleReporter;
buddy_reporting_ConsoleReporter.__name__ = ["buddy","reporting","ConsoleReporter"];
buddy_reporting_ConsoleReporter.__super__ = buddy_reporting_TraceReporter;
buddy_reporting_ConsoleReporter.prototype = $extend(buddy_reporting_TraceReporter.prototype,{
	start: function() {
		return this.resolveImmediately(true);
	}
	,progress: function(spec) {
		this.print((function($this) {
			var $r;
			var _g = spec.status;
			$r = (function($this) {
				var $r;
				switch(_g[1]) {
				case 3:
					$r = "X";
					break;
				case 1:
					$r = ".";
					break;
				case 2:
					$r = "P";
					break;
				case 0:
					$r = "?";
					break;
				}
				return $r;
			}($this));
			return $r;
		}(this)));
		return this.resolveImmediately(spec);
	}
	,done: function(suites,status) {
		var output = buddy_reporting_TraceReporter.prototype.done.call(this,suites,status);
		return output;
	}
	,print: function(s) {
		buddy_internal_sys_Js.print(s);
	}
	,println: function(s) {
		buddy_internal_sys_Js.println(s);
	}
	,__class__: buddy_reporting_ConsoleReporter
});
var buddy_tools_AsyncTools = function() { };
$hxClasses["buddy.tools.AsyncTools"] = buddy_tools_AsyncTools;
buddy_tools_AsyncTools.__name__ = ["buddy","tools","AsyncTools"];
buddy_tools_AsyncTools.iterateAsyncBool = function(it,action) {
	return buddy_tools_AsyncTools.iterateAsync(it,action,true);
};
buddy_tools_AsyncTools.iterateAsync = function(it,action,resolveWith) {
	var finished = new promhx_Deferred();
	var pr = finished.promise();
	buddy_tools_AsyncTools.next($iterator(it)(),action,finished,resolveWith);
	return pr;
};
buddy_tools_AsyncTools.wait = function(ms) {
	var def = new promhx_Deferred();
	var pr = def.promise();
	var done = function() {
		if(!pr._fulfilled) def.resolve(true);
	};
	haxe_Timer.delay(function() {
		done();
	},ms);
	return pr;
};
buddy_tools_AsyncTools.next = function(it,action,def,resolveWith) {
	if(!it.hasNext()) def.resolve(resolveWith); else {
		var n = it.next();
		action(n).then(function(_) {
			buddy_tools_AsyncTools.next(it,action,def,resolveWith);
		});
	}
};
var haxe_StackItem = { __ename__ : true, __constructs__ : ["CFunction","Module","FilePos","Method","LocalFunction"] };
haxe_StackItem.CFunction = ["CFunction",0];
haxe_StackItem.CFunction.toString = $estr;
haxe_StackItem.CFunction.__enum__ = haxe_StackItem;
haxe_StackItem.Module = function(m) { var $x = ["Module",1,m]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.FilePos = function(s,file,line) { var $x = ["FilePos",2,s,file,line]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.Method = function(classname,method) { var $x = ["Method",3,classname,method]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
haxe_StackItem.LocalFunction = function(v) { var $x = ["LocalFunction",4,v]; $x.__enum__ = haxe_StackItem; $x.toString = $estr; return $x; };
var haxe_CallStack = function() { };
$hxClasses["haxe.CallStack"] = haxe_CallStack;
haxe_CallStack.__name__ = ["haxe","CallStack"];
haxe_CallStack.getStack = function(e) {
	if(e == null) return [];
	var oldValue = Error.prepareStackTrace;
	Error.prepareStackTrace = function(error,callsites) {
		var stack = [];
		var _g = 0;
		while(_g < callsites.length) {
			var site = callsites[_g];
			++_g;
			if(haxe_CallStack.wrapCallSite != null) site = haxe_CallStack.wrapCallSite(site);
			var method = null;
			var fullName = site.getFunctionName();
			if(fullName != null) {
				var idx = fullName.lastIndexOf(".");
				if(idx >= 0) {
					var className = HxOverrides.substr(fullName,0,idx);
					var methodName = HxOverrides.substr(fullName,idx + 1,null);
					method = haxe_StackItem.Method(className,methodName);
				}
			}
			stack.push(haxe_StackItem.FilePos(method,site.getFileName(),site.getLineNumber()));
		}
		return stack;
	};
	var a = haxe_CallStack.makeStack(e.stack);
	Error.prepareStackTrace = oldValue;
	return a;
};
haxe_CallStack.exceptionStack = function() {
	return haxe_CallStack.getStack(haxe_CallStack.lastException);
};
haxe_CallStack.makeStack = function(s) {
	if(s == null) return []; else if(typeof(s) == "string") {
		var stack = s.split("\n");
		if(stack[0] == "Error") stack.shift();
		var m = [];
		var rie10 = new EReg("^   at ([A-Za-z0-9_. ]+) \\(([^)]+):([0-9]+):([0-9]+)\\)$","");
		var _g = 0;
		while(_g < stack.length) {
			var line = stack[_g];
			++_g;
			if(rie10.match(line)) {
				var path = rie10.matched(1).split(".");
				var meth = path.pop();
				var file = rie10.matched(2);
				var line1 = Std.parseInt(rie10.matched(3));
				m.push(haxe_StackItem.FilePos(meth == "Anonymous function"?haxe_StackItem.LocalFunction():meth == "Global code"?null:haxe_StackItem.Method(path.join("."),meth),file,line1));
			} else m.push(haxe_StackItem.Module(StringTools.trim(line)));
		}
		return m;
	} else return s;
};
var haxe_IMap = function() { };
$hxClasses["haxe.IMap"] = haxe_IMap;
haxe_IMap.__name__ = ["haxe","IMap"];
var haxe_Log = function() { };
$hxClasses["haxe.Log"] = haxe_Log;
haxe_Log.__name__ = ["haxe","Log"];
haxe_Log.trace = function(v,infos) {
	js_Boot.__trace(v,infos);
};
var haxe_Timer = function(time_ms) {
	var me = this;
	this.id = setInterval(function() {
		me.run();
	},time_ms);
};
$hxClasses["haxe.Timer"] = haxe_Timer;
haxe_Timer.__name__ = ["haxe","Timer"];
haxe_Timer.delay = function(f,time_ms) {
	var t = new haxe_Timer(time_ms);
	t.run = function() {
		t.stop();
		f();
	};
	return t;
};
haxe_Timer.prototype = {
	stop: function() {
		if(this.id == null) return;
		clearInterval(this.id);
		this.id = null;
	}
	,run: function() {
	}
	,__class__: haxe_Timer
};
var haxe_ds_Option = { __ename__ : true, __constructs__ : ["Some","None"] };
haxe_ds_Option.Some = function(v) { var $x = ["Some",0,v]; $x.__enum__ = haxe_ds_Option; $x.toString = $estr; return $x; };
haxe_ds_Option.None = ["None",1];
haxe_ds_Option.None.toString = $estr;
haxe_ds_Option.None.__enum__ = haxe_ds_Option;
var haxe_ds_StringMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.StringMap"] = haxe_ds_StringMap;
haxe_ds_StringMap.__name__ = ["haxe","ds","StringMap"];
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,__class__: haxe_ds_StringMap
};
var haxe_rtti_Meta = function() { };
$hxClasses["haxe.rtti.Meta"] = haxe_rtti_Meta;
haxe_rtti_Meta.__name__ = ["haxe","rtti","Meta"];
haxe_rtti_Meta.getType = function(t) {
	var meta = haxe_rtti_Meta.getMeta(t);
	if(meta == null || meta.obj == null) return { }; else return meta.obj;
};
haxe_rtti_Meta.getMeta = function(t) {
	return t.__meta__;
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
$hxClasses["js._Boot.HaxeError"] = js__$Boot_HaxeError;
js__$Boot_HaxeError.__name__ = ["js","_Boot","HaxeError"];
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
$hxClasses["js.Boot"] = js_Boot;
js_Boot.__name__ = ["js","Boot"];
js_Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
};
js_Boot.__trace = function(v,i) {
	var msg;
	if(i != null) msg = i.fileName + ":" + i.lineNumber + ": "; else msg = "";
	msg += js_Boot.__string_rec(v,"");
	if(i != null && i.customParams != null) {
		var _g = 0;
		var _g1 = i.customParams;
		while(_g < _g1.length) {
			var v1 = _g1[_g];
			++_g;
			msg += "," + js_Boot.__string_rec(v1,"");
		}
	}
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js_Boot.__unhtml(msg) + "<br/>"; else if(typeof console != "undefined" && console.log != null) console.log(msg);
};
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			haxe_CallStack.lastException = e;
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__cast = function(o,t) {
	if(js_Boot.__instanceof(o,t)) return o; else throw new js__$Boot_HaxeError("Cannot cast " + Std.string(o) + " to " + Std.string(t));
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return (Function("return typeof " + name + " != \"undefined\" ? " + name + " : null"))();
};
var promhx_base_AsyncBase = function(d) {
	this._resolved = false;
	this._pending = false;
	this._errorPending = false;
	this._fulfilled = false;
	this._update = [];
	this._error = [];
	this._errored = false;
	if(d != null) promhx_base_AsyncBase.link(d,this,function(x) {
		return x;
	});
};
$hxClasses["promhx.base.AsyncBase"] = promhx_base_AsyncBase;
promhx_base_AsyncBase.__name__ = ["promhx","base","AsyncBase"];
promhx_base_AsyncBase.link = function(current,next,f) {
	current._update.push({ async : next, linkf : function(x) {
		next.handleResolve(f(x));
	}});
	promhx_base_AsyncBase.immediateLinkUpdate(current,next,f);
};
promhx_base_AsyncBase.immediateLinkUpdate = function(current,next,f) {
	if(current._errored) next.handleError(current._errorVal);
	if(current._resolved && !current._pending) try {
		next.handleResolve(f(current._val));
	} catch( e ) {
		haxe_CallStack.lastException = e;
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		next.handleError(e);
	}
};
promhx_base_AsyncBase.linkAll = function(all,next) {
	var cthen = function(arr,current,v) {
		if(arr.length == 0 || promhx_base_AsyncBase.allFulfilled(arr)) {
			var vals;
			var _g = [];
			var $it0 = $iterator(all)();
			while( $it0.hasNext() ) {
				var a = $it0.next();
				_g.push(a == current?v:a._val);
			}
			vals = _g;
			next.handleResolve(vals);
		}
		null;
		return;
	};
	var $it1 = $iterator(all)();
	while( $it1.hasNext() ) {
		var a1 = $it1.next();
		a1._update.push({ async : next, linkf : (function(f,a11,a2) {
			return function(v1) {
				f(a11,a2,v1);
				return;
			};
		})(cthen,(function($this) {
			var $r;
			var _g1 = [];
			var $it2 = $iterator(all)();
			while( $it2.hasNext() ) {
				var a21 = $it2.next();
				if(a21 != a1) _g1.push(a21);
			}
			$r = _g1;
			return $r;
		}(this)),a1)});
	}
	if(promhx_base_AsyncBase.allFulfilled(all)) next.handleResolve((function($this) {
		var $r;
		var _g2 = [];
		var $it3 = $iterator(all)();
		while( $it3.hasNext() ) {
			var a3 = $it3.next();
			_g2.push(a3._val);
		}
		$r = _g2;
		return $r;
	}(this)));
};
promhx_base_AsyncBase.pipeLink = function(current,ret,f) {
	var linked = false;
	var linkf = function(x) {
		if(!linked) {
			linked = true;
			var pipe_ret = f(x);
			pipe_ret._update.push({ async : ret, linkf : $bind(ret,ret.handleResolve)});
			promhx_base_AsyncBase.immediateLinkUpdate(pipe_ret,ret,function(x1) {
				return x1;
			});
		}
	};
	current._update.push({ async : ret, linkf : linkf});
	if(current._resolved && !current._pending) try {
		linkf(current._val);
	} catch( e ) {
		haxe_CallStack.lastException = e;
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		ret.handleError(e);
	}
};
promhx_base_AsyncBase.allResolved = function($as) {
	var $it0 = $iterator($as)();
	while( $it0.hasNext() ) {
		var a = $it0.next();
		if(!a._resolved) return false;
	}
	return true;
};
promhx_base_AsyncBase.allFulfilled = function($as) {
	var $it0 = $iterator($as)();
	while( $it0.hasNext() ) {
		var a = $it0.next();
		if(!a._fulfilled) return false;
	}
	return true;
};
promhx_base_AsyncBase.prototype = {
	catchError: function(f) {
		this._error.push(f);
		return this;
	}
	,errorThen: function(f) {
		this._errorMap = f;
		return this;
	}
	,isResolved: function() {
		return this._resolved;
	}
	,isErrored: function() {
		return this._errored;
	}
	,isFulfilled: function() {
		return this._fulfilled;
	}
	,isPending: function() {
		return this._pending;
	}
	,handleResolve: function(val) {
		this._resolve(val);
	}
	,_resolve: function(val) {
		var _g = this;
		if(this._pending) promhx_base_EventLoop.enqueue((function(f,a1) {
			return function() {
				f(a1);
			};
		})($bind(this,this._resolve),val)); else {
			this._resolved = true;
			this._pending = true;
			promhx_base_EventLoop.queue.add(function() {
				_g._val = val;
				var _g1 = 0;
				var _g2 = _g._update;
				while(_g1 < _g2.length) {
					var up = _g2[_g1];
					++_g1;
					try {
						up.linkf(val);
					} catch( e ) {
						haxe_CallStack.lastException = e;
						if (e instanceof js__$Boot_HaxeError) e = e.val;
						up.async.handleError(e);
					}
				}
				_g._fulfilled = true;
				_g._pending = false;
			});
			promhx_base_EventLoop.continueOnNextLoop();
		}
	}
	,handleError: function(error) {
		this._handleError(error);
	}
	,_handleError: function(error) {
		var _g = this;
		var update_errors = function(e) {
			if(_g._error.length > 0) {
				var _g1 = 0;
				var _g2 = _g._error;
				while(_g1 < _g2.length) {
					var ef = _g2[_g1];
					++_g1;
					ef(e);
				}
			} else if(_g._update.length > 0) {
				var _g11 = 0;
				var _g21 = _g._update;
				while(_g11 < _g21.length) {
					var up = _g21[_g11];
					++_g11;
					up.async.handleError(e);
				}
			} else throw new js__$Boot_HaxeError(e);
			_g._errorPending = false;
		};
		if(!this._errorPending) {
			this._errorPending = true;
			this._errored = true;
			this._errorVal = error;
			promhx_base_EventLoop.queue.add(function() {
				if(_g._errorMap != null) try {
					_g._resolve(_g._errorMap(error));
				} catch( e1 ) {
					haxe_CallStack.lastException = e1;
					if (e1 instanceof js__$Boot_HaxeError) e1 = e1.val;
					update_errors(e1);
				} else update_errors(error);
			});
			promhx_base_EventLoop.continueOnNextLoop();
		}
	}
	,then: function(f) {
		var ret = new promhx_base_AsyncBase();
		promhx_base_AsyncBase.link(this,ret,f);
		return ret;
	}
	,unlink: function(to) {
		var _g = this;
		promhx_base_EventLoop.queue.add(function() {
			_g._update = _g._update.filter(function(x) {
				return x.async != to;
			});
		});
		promhx_base_EventLoop.continueOnNextLoop();
	}
	,isLinked: function(to) {
		var updated = false;
		var _g = 0;
		var _g1 = this._update;
		while(_g < _g1.length) {
			var u = _g1[_g];
			++_g;
			if(u.async == to) return true;
		}
		return updated;
	}
	,__class__: promhx_base_AsyncBase
};
var promhx_Deferred = $hx_exports.promhx.Deferred = function() {
	promhx_base_AsyncBase.call(this);
};
$hxClasses["promhx.Deferred"] = promhx_Deferred;
promhx_Deferred.__name__ = ["promhx","Deferred"];
promhx_Deferred.__super__ = promhx_base_AsyncBase;
promhx_Deferred.prototype = $extend(promhx_base_AsyncBase.prototype,{
	resolve: function(val) {
		this.handleResolve(val);
	}
	,throwError: function(e) {
		this.handleError(e);
	}
	,promise: function() {
		return new promhx_Promise(this);
	}
	,stream: function() {
		return new promhx_Stream(this);
	}
	,publicStream: function() {
		return new promhx_PublicStream(this);
	}
	,__class__: promhx_Deferred
});
var promhx_Promise = $hx_exports.promhx.Promise = function(d) {
	promhx_base_AsyncBase.call(this,d);
	this._rejected = false;
};
$hxClasses["promhx.Promise"] = promhx_Promise;
promhx_Promise.__name__ = ["promhx","Promise"];
promhx_Promise.whenAll = function(itb) {
	var ret = new promhx_Promise();
	promhx_base_AsyncBase.linkAll(itb,ret);
	return ret;
};
promhx_Promise.promise = function(_val) {
	var ret = new promhx_Promise();
	ret.handleResolve(_val);
	return ret;
};
promhx_Promise.__super__ = promhx_base_AsyncBase;
promhx_Promise.prototype = $extend(promhx_base_AsyncBase.prototype,{
	isRejected: function() {
		return this._rejected;
	}
	,reject: function(e) {
		this._rejected = true;
		this.handleError(e);
	}
	,handleResolve: function(val) {
		if(this._resolved) {
			var msg = "Promise has already been resolved";
			throw new js__$Boot_HaxeError(promhx_error_PromiseError.AlreadyResolved(msg));
		}
		this._resolve(val);
	}
	,then: function(f) {
		var ret = new promhx_Promise();
		promhx_base_AsyncBase.link(this,ret,f);
		return ret;
	}
	,unlink: function(to) {
		var _g = this;
		promhx_base_EventLoop.queue.add(function() {
			if(!_g._fulfilled) {
				var msg = "Downstream Promise is not fullfilled";
				_g.handleError(promhx_error_PromiseError.DownstreamNotFullfilled(msg));
			} else _g._update = _g._update.filter(function(x) {
				return x.async != to;
			});
		});
		promhx_base_EventLoop.continueOnNextLoop();
	}
	,handleError: function(error) {
		this._rejected = true;
		this._handleError(error);
	}
	,pipe: function(f) {
		var ret = new promhx_Promise();
		promhx_base_AsyncBase.pipeLink(this,ret,f);
		return ret;
	}
	,errorPipe: function(f) {
		var ret = new promhx_Promise();
		this.catchError(function(e) {
			var piped = f(e);
			piped.then($bind(ret,ret._resolve));
		});
		this.then($bind(ret,ret._resolve));
		return ret;
	}
	,__class__: promhx_Promise
});
var promhx_Stream = $hx_exports.promhx.Stream = function(d) {
	promhx_base_AsyncBase.call(this,d);
	this._end_deferred = new promhx_Deferred();
	this._end_promise = this._end_deferred.promise();
};
$hxClasses["promhx.Stream"] = promhx_Stream;
promhx_Stream.__name__ = ["promhx","Stream"];
promhx_Stream.foreach = function(itb) {
	var s = new promhx_Stream();
	var $it0 = $iterator(itb)();
	while( $it0.hasNext() ) {
		var i = $it0.next();
		s.handleResolve(i);
	}
	s.end();
	return s;
};
promhx_Stream.wheneverAll = function(itb) {
	var ret = new promhx_Stream();
	promhx_base_AsyncBase.linkAll(itb,ret);
	return ret;
};
promhx_Stream.concatAll = function(itb) {
	var ret = new promhx_Stream();
	var $it0 = $iterator(itb)();
	while( $it0.hasNext() ) {
		var i = $it0.next();
		ret.concat(i);
	}
	return ret;
};
promhx_Stream.mergeAll = function(itb) {
	var ret = new promhx_Stream();
	var $it0 = $iterator(itb)();
	while( $it0.hasNext() ) {
		var i = $it0.next();
		ret.merge(i);
	}
	return ret;
};
promhx_Stream.stream = function(_val) {
	var ret = new promhx_Stream();
	ret.handleResolve(_val);
	return ret;
};
promhx_Stream.__super__ = promhx_base_AsyncBase;
promhx_Stream.prototype = $extend(promhx_base_AsyncBase.prototype,{
	then: function(f) {
		var ret = new promhx_Stream();
		promhx_base_AsyncBase.link(this,ret,f);
		this._end_promise.then(function(x) {
			ret.end();
		});
		return ret;
	}
	,detachStream: function(str) {
		var filtered = [];
		var removed = false;
		var _g = 0;
		var _g1 = this._update;
		while(_g < _g1.length) {
			var u = _g1[_g];
			++_g;
			if(u.async == str) removed = true; else filtered.push(u);
		}
		this._update = filtered;
		return removed;
	}
	,first: function() {
		var s = new promhx_Promise();
		this.then(function(x) {
			if(!s._resolved) s.handleResolve(x);
		});
		return s;
	}
	,handleResolve: function(val) {
		if(!this._end && !this._pause) this._resolve(val);
	}
	,pause: function(set) {
		if(set == null) set = !this._pause;
		this._pause = set;
	}
	,pipe: function(f) {
		var ret = new promhx_Stream();
		promhx_base_AsyncBase.pipeLink(this,ret,f);
		this._end_promise.then(function(x) {
			ret.end();
		});
		return ret;
	}
	,errorPipe: function(f) {
		var ret = new promhx_Stream();
		this.catchError(function(e) {
			var piped = f(e);
			piped.then($bind(ret,ret._resolve));
			piped._end_promise.then(($_=ret._end_promise,$bind($_,$_._resolve)));
		});
		this.then($bind(ret,ret._resolve));
		this._end_promise.then(function(x) {
			ret.end();
		});
		return ret;
	}
	,handleEnd: function() {
		if(this._pending) {
			promhx_base_EventLoop.queue.add($bind(this,this.handleEnd));
			promhx_base_EventLoop.continueOnNextLoop();
		} else if(this._end_promise._resolved) return; else {
			this._end = true;
			var o;
			if(this._resolved) o = haxe_ds_Option.Some(this._val); else o = haxe_ds_Option.None;
			this._end_promise.handleResolve(o);
			this._update = [];
			this._error = [];
		}
	}
	,end: function() {
		promhx_base_EventLoop.queue.add($bind(this,this.handleEnd));
		promhx_base_EventLoop.continueOnNextLoop();
		return this;
	}
	,endThen: function(f) {
		return this._end_promise.then(f);
	}
	,filter: function(f) {
		var ret = new promhx_Stream();
		this._update.push({ async : ret, linkf : function(x) {
			if(f(x)) ret.handleResolve(x);
		}});
		promhx_base_AsyncBase.immediateLinkUpdate(this,ret,function(x1) {
			return x1;
		});
		return ret;
	}
	,concat: function(s) {
		var ret = new promhx_Stream();
		this._update.push({ async : ret, linkf : $bind(ret,ret.handleResolve)});
		promhx_base_AsyncBase.immediateLinkUpdate(this,ret,function(x) {
			return x;
		});
		this._end_promise.then(function(_) {
			s.pipe(function(x1) {
				ret.handleResolve(x1);
				return ret;
			});
			s._end_promise.then(function(_1) {
				ret.end();
			});
		});
		return ret;
	}
	,merge: function(s) {
		var ret = new promhx_Stream();
		this._update.push({ async : ret, linkf : $bind(ret,ret.handleResolve)});
		s._update.push({ async : ret, linkf : $bind(ret,ret.handleResolve)});
		promhx_base_AsyncBase.immediateLinkUpdate(this,ret,function(x) {
			return x;
		});
		promhx_base_AsyncBase.immediateLinkUpdate(s,ret,function(x1) {
			return x1;
		});
		return ret;
	}
	,__class__: promhx_Stream
});
var promhx_PublicStream = $hx_exports.promhx.PublicStream = function(def) {
	promhx_Stream.call(this,def);
};
$hxClasses["promhx.PublicStream"] = promhx_PublicStream;
promhx_PublicStream.__name__ = ["promhx","PublicStream"];
promhx_PublicStream.publicstream = function(val) {
	var ps = new promhx_PublicStream();
	ps.handleResolve(val);
	return ps;
};
promhx_PublicStream.__super__ = promhx_Stream;
promhx_PublicStream.prototype = $extend(promhx_Stream.prototype,{
	resolve: function(val) {
		this.handleResolve(val);
	}
	,throwError: function(e) {
		this.handleError(e);
	}
	,update: function(val) {
		this.handleResolve(val);
	}
	,__class__: promhx_PublicStream
});
var promhx_base_EventLoop = function() { };
$hxClasses["promhx.base.EventLoop"] = promhx_base_EventLoop;
promhx_base_EventLoop.__name__ = ["promhx","base","EventLoop"];
promhx_base_EventLoop.enqueue = function(eqf) {
	promhx_base_EventLoop.queue.add(eqf);
	promhx_base_EventLoop.continueOnNextLoop();
};
promhx_base_EventLoop.set_nextLoop = function(f) {
	if(promhx_base_EventLoop.nextLoop != null) throw new js__$Boot_HaxeError("nextLoop has already been set"); else promhx_base_EventLoop.nextLoop = f;
	return promhx_base_EventLoop.nextLoop;
};
promhx_base_EventLoop.queueEmpty = function() {
	return promhx_base_EventLoop.queue.isEmpty();
};
promhx_base_EventLoop.finish = function(max_iterations) {
	if(max_iterations == null) max_iterations = 1000;
	var fn = null;
	while(max_iterations-- > 0 && (fn = promhx_base_EventLoop.queue.pop()) != null) fn();
	return promhx_base_EventLoop.queue.isEmpty();
};
promhx_base_EventLoop.clear = function() {
	promhx_base_EventLoop.queue = new List();
};
promhx_base_EventLoop.f = function() {
	var fn = promhx_base_EventLoop.queue.pop();
	if(fn != null) fn();
	if(!promhx_base_EventLoop.queue.isEmpty()) promhx_base_EventLoop.continueOnNextLoop();
};
promhx_base_EventLoop.continueOnNextLoop = function() {
	if(promhx_base_EventLoop.nextLoop != null) promhx_base_EventLoop.nextLoop(promhx_base_EventLoop.f); else setImmediate(promhx_base_EventLoop.f);
};
var promhx_error_PromiseError = { __ename__ : true, __constructs__ : ["AlreadyResolved","DownstreamNotFullfilled"] };
promhx_error_PromiseError.AlreadyResolved = function(message) { var $x = ["AlreadyResolved",0,message]; $x.__enum__ = promhx_error_PromiseError; $x.toString = $estr; return $x; };
promhx_error_PromiseError.DownstreamNotFullfilled = function(message) { var $x = ["DownstreamNotFullfilled",1,message]; $x.__enum__ = promhx_error_PromiseError; $x.toString = $estr; return $x; };
var utils_Table = function() { };
$hxClasses["utils.Table"] = utils_Table;
utils_Table.__name__ = ["utils","Table"];
utils_Table.dateFormatter = function(value,row) {
	return value.split(" ")[0];
};
utils_Table.currencyFormatter = function(value,row) {
	if(!utils_Table.startsWithNumber.match(value)) value = value.substring(1);
	var intValue = utils_Utils.stringToFloat(value);
	if(intValue == null) intValue = 0;
	if(intValue == null) value = "null"; else value = "" + intValue;
	return "€ " + value;
};
utils_Table.intFormatter = function(value,row) {
	if(Std.parseInt(value) == null) return 0;
	return Std.parseInt(value);
};
var utils_Utils = function() { };
$hxClasses["utils.Utils"] = utils_Utils;
utils_Utils.__name__ = ["utils","Utils"];
utils_Utils.flashMessage = function(msg,type,align,allowDismiss) {
	if(allowDismiss == null) allowDismiss = true;
	if(align == null) align = "right";
	if(type != "info" && type != "danger" && type != "success" && type != null) type = "info";
	js.JQuery.bootstrapGrowl(msg,{ type : type, width : "auto", align : align, allow_dismiss : allowDismiss});
};
utils_Utils.createDOMFromString = function(s) {
	return js.JQuery.parseHTML(s);
};
utils_Utils.handleUfError = function(data,status) {
	var errMsg = utils_Utils.getUfrontError(data);
	utils_Utils.flashMessage(errMsg.title != null?errMsg.title:errMsg.msg,"danger");
	haxe_Log.trace(errMsg,{ fileName : "Utils.hx", lineNumber : 52, className : "utils.Utils", methodName : "handleUfError"});
};
utils_Utils.getUfrontError = function(htmlError) {
	var jq = js.JQuery("<div>" + (htmlError.responseText + "</div>"));
	var title = jq.find(".error-data");
	var msg = jq.find(".error-message");
	var pos = jq.find(".error-pos");
	var stack = jq.find("code");
	return { title : title.text(), msg : msg.text(), pos : null, stack : null, raw : htmlError};
};
utils_Utils.centerDialog = function(modal) {
	modal.css("display","block");
	var dialog = modal.find(".modal-dialog");
	var content = modal.find(".modal-content");
	var height;
	height = modal.height() / 2 - dialog.height() / 2;
	dialog.css("margin-top",height);
};
utils_Utils.stringToFloat = function(str) {
	var f = parseFloat(str);
	if((function($this) {
		var $r;
		var f1;
		f1 = js_Boot.__cast(f , Float);
		$r = isNaN(f1);
		return $r;
	}(this))) return null; else return f;
};
utils_Utils.stringToJQuery = function(html) {
	return js.JQuery(js.JQuery.parseHTML(html));
};
utils_Utils.jcache = function(s,refresh) {
	if(refresh == null) refresh = false;
	var el = utils_Utils._JCache.get(s);
	if(el == null || refresh) {
		el = js.JQuery(s);
		if(el != null) utils_Utils._JCache.set(s,el);
	}
	return el;
};
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; }
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
$hxClasses.Math = Math;
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
$hxClasses.Array = Array;
Array.__name__ = ["Array"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
if(Array.prototype.filter == null) Array.prototype.filter = function(f1) {
	var a1 = [];
	var _g11 = 0;
	var _g2 = this.length;
	while(_g11 < _g2) {
		var i1 = _g11++;
		var e = this[i1];
		if(f1(e)) a1.push(e);
	}
	return a1;
};
var __map_reserved = {}
var q = window.jQuery;
var js = js || {}
js.JQuery = q;
var global = window;
(function (global, undefined) {
    "use strict";

    var tasks = (function () {
        function Task(handler, args) {
            this.handler = handler;
            this.args = args;
        }
        Task.prototype.run = function () {
            // See steps in section 5 of the spec.
            if (typeof this.handler === "function") {
                // Choice of `thisArg` is not in the setImmediate spec; `undefined` is in the setTimeout spec though:
                // http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html
                this.handler.apply(undefined, this.args);
            } else {
                var scriptSource = "" + this.handler;
                /*jshint evil: true */
                eval(scriptSource);
            }
        };

        var nextHandle = 1; // Spec says greater than zero
        var tasksByHandle = {};
        var currentlyRunningATask = false;

        return {
            addFromSetImmediateArguments: function (args) {
                var handler = args[0];
                var argsToHandle = Array.prototype.slice.call(args, 1);
                var task = new Task(handler, argsToHandle);

                var thisHandle = nextHandle++;
                tasksByHandle[thisHandle] = task;
                return thisHandle;
            },
            runIfPresent: function (handle) {
                // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
                // So if we're currently running a task, we'll need to delay this invocation.
                if (!currentlyRunningATask) {
                    var task = tasksByHandle[handle];
                    if (task) {
                        currentlyRunningATask = true;
                        try {
                            task.run();
                        } finally {
                            delete tasksByHandle[handle];
                            currentlyRunningATask = false;
                        }
                    }
                } else {
                    // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
                    // "too much recursion" error.
                    global.setTimeout(function () {
                        tasks.runIfPresent(handle);
                    }, 0);
                }
            },
            remove: function (handle) {
                delete tasksByHandle[handle];
            }
        };
    }());

    function canUseNextTick() {
        // Don't get fooled by e.g. browserify environments.
        return typeof process === "object" &&
               Object.prototype.toString.call(process) === "[object process]";
    }

    function canUseMessageChannel() {
        return !!global.MessageChannel;
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.

        if (!global.postMessage || global.importScripts) {
            return false;
        }

        var postMessageIsAsynchronous = true;
        var oldOnMessage = global.onmessage;
        global.onmessage = function () {
            postMessageIsAsynchronous = false;
        };
        global.postMessage("", "*");
        global.onmessage = oldOnMessage;

        return postMessageIsAsynchronous;
    }

    function canUseReadyStateChange() {
        return "document" in global && "onreadystatechange" in global.document.createElement("script");
    }

    function installNextTickImplementation(attachTo) {
        attachTo.setImmediate = function () {
            var handle = tasks.addFromSetImmediateArguments(arguments);

            process.nextTick(function () {
                tasks.runIfPresent(handle);
            });

            return handle;
        };
    }

    function installMessageChannelImplementation(attachTo) {
        var channel = new global.MessageChannel();
        channel.port1.onmessage = function (event) {
            var handle = event.data;
            tasks.runIfPresent(handle);
        };
        attachTo.setImmediate = function () {
            var handle = tasks.addFromSetImmediateArguments(arguments);

            channel.port2.postMessage(handle);

            return handle;
        };
    }

    function installPostMessageImplementation(attachTo) {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var MESSAGE_PREFIX = "com.bn.NobleJS.setImmediate" + Math.random();

        function isStringAndStartsWith(string, putativeStart) {
            return typeof string === "string" && string.substring(0, putativeStart.length) === putativeStart;
        }

        function onGlobalMessage(event) {
            // This will catch all incoming messages (even from other windows!), so we need to try reasonably hard to
            // avoid letting anyone else trick us into firing off. We test the origin is still this window, and that a
            // (randomly generated) unpredictable identifying prefix is present.
            if (event.source === global && isStringAndStartsWith(event.data, MESSAGE_PREFIX)) {
                var handle = event.data.substring(MESSAGE_PREFIX.length);
                tasks.runIfPresent(handle);
            }
        }
        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        attachTo.setImmediate = function () {
            var handle = tasks.addFromSetImmediateArguments(arguments);

            // Make `global` post a message to itself with the handle and identifying prefix, thus asynchronously
            // invoking our onGlobalMessage listener above.
            global.postMessage(MESSAGE_PREFIX + handle, "*");

            return handle;
        };
    }

    function installReadyStateChangeImplementation(attachTo) {
        attachTo.setImmediate = function () {
            var handle = tasks.addFromSetImmediateArguments(arguments);

            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var scriptEl = global.document.createElement("script");
            scriptEl.onreadystatechange = function () {
                tasks.runIfPresent(handle);

                scriptEl.onreadystatechange = null;
                scriptEl.parentNode.removeChild(scriptEl);
                scriptEl = null;
            };
            global.document.documentElement.appendChild(scriptEl);

            return handle;
        };
    }

    function installSetTimeoutImplementation(attachTo) {
        attachTo.setImmediate = function () {
            var handle = tasks.addFromSetImmediateArguments(arguments);

            global.setTimeout(function () {
                tasks.runIfPresent(handle);
            }, 0);

            return handle;
        };
    }

    if (!global.setImmediate) {
        // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
        var attachTo = typeof Object.getPrototypeOf === "function" && "setTimeout" in Object.getPrototypeOf(global) ?
                          Object.getPrototypeOf(global)
                        : global;

        if (canUseNextTick()) {
            // For Node.js before 0.9
            installNextTickImplementation(attachTo);
        } else if (canUsePostMessage()) {
            // For non-IE10 modern browsers
            installPostMessageImplementation(attachTo);
        } else if (canUseMessageChannel()) {
            // For web workers, where supported
            installMessageChannelImplementation(attachTo);
        } else if (canUseReadyStateChange()) {
            // For IE 6–8
            installReadyStateChangeImplementation(attachTo);
        } else {
            // For older browsers
            installSetTimeoutImplementation(attachTo);
        }

        attachTo.clearImmediate = tasks.remove;
    }
}(typeof global === "object" && global ? global : this));
;
buddy_BuddySuite.exclude = "exclude";
buddy_BuddySuite.include = "include";
TestClient.__meta__ = { obj : { autoIncluded : ["TestClient"]}};
js_Boot.__toStr = {}.toString;
promhx_base_EventLoop.queue = new List();
utils_Table.startsWithNumber = new EReg("^[0-9-.]","");
utils_Utils._JCache = new haxe_ds_StringMap();
TestClient.main();
})(typeof console != "undefined" ? console : {log:function(){}}, typeof window != "undefined" ? window : exports);
