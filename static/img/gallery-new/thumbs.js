'use strict'
let fs = require('fs')
let gm = require('gm')
let quality = 90
let size = {
  width: 350,
  height: 250
}

function endsWith (str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1
}

let files = fs.readdirSync('.')
files = files.filter(file => {
  return endsWith(file, '.jpg') && !endsWith(file, 'thumb.jpg')
})

for (let file of files) {
  let filename = file.slice(0, file.length - 4)
  console.log('creating: ' + filename + '-thumb.jpg')
  // equivalent to: gm convert inputPath -resize "200x200>" -gravity center -extent 200x200 outputPath
  gm(file)
  .resize(size.width, size.height + '^')
  .gravity('Center')
  .extent(size.width, size.height)
  .quality(quality)
  .write(filename + '-thumb.jpg', function (err) {
    if (err) {
      console.log('Error Occured')
      console.log(err)
    }
  })
}

// remove files extension
files = files.map(file => file.slice(0, file.length - 4))
// write files list to json file
fs.writeFileSync('images-list.json', JSON.stringify(files))

